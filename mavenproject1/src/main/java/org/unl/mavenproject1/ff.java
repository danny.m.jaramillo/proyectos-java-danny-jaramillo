/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unl.mavenproject1;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class ff {

    public static void main(String[] args) {
        int numero;
        System.out.println(" PRIMERA PARTE: DECLARAR UNA MATRIZ BIDIMENCIONAL Y DETERMINAR CUANTOS NUMEROS SON MULTIPLOS DE 5 Y  POSICIONARLOS EN UNA MATRIZ UNIDIMENCIONAL.");

        Scanner lector = new Scanner(System.in);
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" Ingrese un Limite de la Matriz  Cuadrada.");
        System.out.println("..........................................");
        numero = lector.nextInt();
        System.out.println("-----------------------------------------");
        int cont = 0;
//        Declaro la Matriz Bidimencional
        int[][] a = new int[numero][numero];
//     Declaro una variable para determinar cuantos multiplos de 5 encontramos en la Matriz Bidimencional

        int tamaño = 0;
// Recorremos la matriz
        for (int i = 0; i < numero; i++) {
            for (int j = 0; j < numero; j++) {
                a[i][j] = (int) Math.floor(Math.random() * (100 - 0)) + 0;
                //El texto se emantiene en la misma linea
                System.out.print("[" + a[i][j] + "] -  ");

//                Determinamos si el numero generado es multiplod e 5
                if (a[i][j] % 5 == 0) {

                    tamaño++;
                }
                if (cont == (numero - 1)) {
                    //El texto realiza un salto de linea

                    System.out.println("  ");
                }
                cont++;
            }
            cont = 0;

        }
        System.out.println("-------------------------");
        System.out.println("La cantidad de numeros Multiplos de 5 que contiene la Matriz Bidimencional son: ");
        System.out.println(" " + tamaño);
        // Declaro la Matriz Unidimencional
        int[] multcinco = new int[tamaño];
//        Recorro nuevamente la amtris Bidimencional para extraer los numeros Multiplos de 5
//Decalro un cantador para determianr la posicion en la caul  uvico el valor  , yaque puede darce casos en donde en un finla no existan numeros multiplos de 5.
        int contUn = 0;
        for (int i = 0; i < numero; i++) {
            for (int j = 0; j < numero; j++) {

                if (a[i][j] % 5 == 0) {

                    multcinco[contUn] = a[i][j];
                    contUn++;

                }

            }

        }
//        Recorro la  Matris Unidimencional
        System.out.println("------------------------------------------------");
        System.out.println(" Los Elementos de la Matriz Unidimencional son :");
        for (int i = 0; i < (tamaño); i++) {

            System.out.print(" [" + multcinco[i] + "] -");

        }
        System.out.println("    ");
        System.out.println("    ");

        System.out.println(" SEGUNDA PARTE : RECORRER LA DIAGONAL INVERTIDA DE LA MATRIZ BIDIMIENCIONAL ANTERIORMENTE DECLARA Y SUMAR SUS ELEMENTOS.");

        int[] inversa = new int[numero];
        int continversa = 0;
        for (int i = 0; i < numero; i++) {
            for (int j = 0; j < numero; j++) {

                if ((i + j) == (numero - 1)) {
                    inversa[continversa] = a[i][j];
                    continversa++;
                }

            }

        }
        System.out.println("");
        System.out.println(" La Diagonal Inversa de la Matriz es :");
        for (int i = 0; i < (numero); i++) {

            System.out.print(" [" + inversa[i] + "] -");

        }
        System.out.println("");
        System.out.println("");
        System.out.println("El Resultado de la suma de sus Elementos es :");
        int suma = 0;
        for (int i = 0; i < (numero); i++) {

            suma = suma + inversa[i];
           

        }
         System.out.println("  "+suma);
    }
}
