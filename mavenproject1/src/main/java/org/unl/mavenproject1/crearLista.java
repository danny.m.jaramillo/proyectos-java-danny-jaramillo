/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unl.mavenproject1;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class crearLista {

    public static void main(String[] args) {
        //comboco al metodo como es STATIC  solo se llaman entre metodos estativos (nombre de la clase (.)nonbre del metodo)
//        crearLista.crearLista();
        // agrego y leo la lista 
        crearLista.generarMatriz(10);
    }

    public static void crearLista() {
//      Integer ambio a Int
        Integer entero = new Integer(5);
        int z = entero.intValue();
//        CASTING
        Object o = entero;
        Object o2 = new Boolean(true);
        Integer entero2 = (Integer) o;
        List ejemplo = new ArrayList();
        ejemplo.add(new Boolean(true));
        ejemplo.add(new String("kajkdj"));
       
        //...................
        Integer a = new Integer(5);
        Integer b = new Integer(10);
        Integer c = new Integer(15);
        Integer d = new Integer(6);
        Integer e = new Integer(15);
//        INDEFINIDA CAULQUIER VALOR SE INGRESA 
//        List lista = new ArrayList();
//        DEFINIDA SOLO VALORES DEL TIPO QUE PEDIMOS
        List<Integer> lista = new ArrayList<Integer>();
        lista.add(a);
        lista.add(b);
        lista.add(c);
//        lista.add(new Boolean(true));
        for (int i = 0; i < lista.size(); i++) {
            //SOLO PUEDO EXTRAER SI DECLARO INTEGER EL EMBOLVENTE YAQUE LA LISTA LA DECLARE COMO INTEGER 
            Integer numActual = lista.get(i);
            System.out.println(lista.get(i));
        }
////        Lista tienes al objeto devuelbe true o falce
        System.out.println(lista.contains(a));
        System.out.println(lista.contains(b));

        //Imprimer la posicion de un objeto exacto 
        System.out.println(lista.indexOf(c));
        System.out.println(lista.indexOf(d));
        System.out.println(lista.indexOf(e));
    }

    public static void generarMatriz(int tamano) {
        List lista = new ArrayList();
        List ejemplo = new ArrayList();
        //agregar una lista a otra lista
         lista.addAll(ejemplo);
        //AGREGO
        for (int i = 0; i < tamano; i++) {
            int valor = ((int) (Math.random() * 10));
            //para que no se repitan 
            if (!lista.contains(new Integer(valor))) {
                lista.add(valor);
            }
        }
        //LEO recorro LA LIST
        for (int i = 0; i < lista.size(); i++) {
            System.out.println(lista.get(i));
        }

    }
}
