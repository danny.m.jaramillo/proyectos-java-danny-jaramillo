/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Danny Jaramillo
 */
public class Asignatura {
    private String nombre;
    private int cilo;
    private int capasidaddeestudiantes;
    private String prerequisitos;
    private int numerocreditos;
    private int nota ;
    
  
    
    private int matriculados;

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the cilo
     */
    public int getCilo() {
        return cilo;
    }

    /**
     * @param cilo the cilo to set
     */
    public void setCilo(int cilo) {
        this.cilo = cilo;
    }

    /**
     * @return the capasidaddeestudiantes
     */
    public int getCapasidaddeestudiantes() {
        return capasidaddeestudiantes;
    }

    /**
     * @param capasidaddeestudiantes the capasidaddeestudiantes to set
     */
    public void setCapasidaddeestudiantes(int capasidaddeestudiantes) {
        this.capasidaddeestudiantes = capasidaddeestudiantes;
    }

    /**
     * @return the prerequisitos
     */
    public String getPrerequisitos() {
        return prerequisitos;
    }

    /**
     * @param prerequisitos the prerequisitos to set
     */
    public void setPrerequisitos(String prerequisitos) {
        this.prerequisitos = prerequisitos;
    }

    /**
     * @return the numerocreditos
     */
    public int getNumerocreditos() {
        return numerocreditos;
    }

    /**
     * @param numerocreditos the numerocreditos to set
     */
    public void setNumerocreditos(int numerocreditos) {
        this.numerocreditos = numerocreditos;
    }

    /**
     * @return the matriculados
     */
    public int getMatriculados() {
        return matriculados;
    }

    /**
     * @param matriculados the matriculados to set
     */
    public void setMatriculados(int matriculados) {
        this.matriculados = matriculados;
    }

    /**
     * @return the nota
     */
    public int getNota() {
        return nota;
    }

    /**
     * @param nota the nota to set
     */
    public void setNota(int nota) {
        this.nota = nota;
    }
    
    
}
