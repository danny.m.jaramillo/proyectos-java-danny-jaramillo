/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Modelo.*;
import java.util.ArrayList;

/**
 *
 * @author Danny Jaramillo
 */
public class Estudiante {

    private java.util.List<Asignatura> listaasignatura = new ArrayList<>();

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the cedula
     */
    public int getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(int cedula) {
        this.cedula = cedula;
    }
    private String nombre;
    private int cedula;

    /**
     * @return the listaasignatura
     */
    public java.util.List<Asignatura> getListaasignatura() {
        return listaasignatura;
    }

    /**
     * @param listaasignatura the listaasignatura to set
     */
    public void setListaasignatura(java.util.List<Asignatura> listaasignatura) {
        this.listaasignatura = listaasignatura;
    }

    /**
     * @return the listaestudiante
     */
   

}
