/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metodos_Ordenacion;

import java.util.Scanner;

/**
 *
 * @author Danny Jaramillo
 */
public class Primos {

    public static  int prim(int num) {

        
        int x=(int)Math.sqrt(num);
        for(int i =2;i<=x;i++){
           
            if(num % i == 0){
                return i;
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        
        
        Scanner pantalla = new Scanner(System.in);
        System.out.println("Ingrese el numero para determinar si es primo o no es primo");
        System.out.println("-----------------------------------------------------------");
        int numero = pantalla.nextInt();
        System.out.println(" Si retorna 0 es Primo , Si retorna otro numero es diferente de 0 es Compuesto");
        System.out.println("Retorno "+ prim(numero));
       
    }

}
