/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matematicasdiscretas;

import java.math.BigInteger;
import java.util.Scanner;

//llave publica 
//
/**
 *
 * @author Danny Jaramillo
 */
public class Encriptacion {

    public static void main(String[] args) {
        System.out.println("******************************************************************************");
        System.out.println("*SISTEMA DE ENCRIPTACION BASADO EN RSA Y EL USO DE LLAVES PUBLICAS Y PRIVADAS*");
        System.out.println("******************************************************************************");
        System.out.println(" ");

        Scanner pantalla = new Scanner(System.in);
        System.out.println("----------------------------------");
        System.out.println("| Ingrese el mensaje a encriptar :|");
        System.out.println("----------------------------------");
        String valor_Ingresado = pantalla.nextLine();

        char separado[] = valor_Ingresado.toCharArray();

        int x = 11;
        int xc = 49;
        //Coprimos / yel modulo
        int c = (int) Math.pow(x, 2) % 2;
        int c1 = (int) Math.pow(xc, 2) % 2;
        int b = c + c1;

        System.out.println("---------------------");
        System.out.println(" Numeros encriptados ");
        System.out.println("---------------------");
        String ji = "";
        for (int i = 0; i < separado.length; i++) {

            int sd = (int) separado[i];
            int op = (int) Math.pow(sd, 9);
            BigInteger xn = new BigInteger(op + "");

            System.out.println(separado[i] + "= " + xn);
            separado[i] = (char) (separado[i] + (char) (b));
            ji = ji + op + "";

        }

        System.out.println("");
        System.out.println("Codigo total " + ji);

        System.out.println("");
        String desconocido = String.valueOf(separado);
        System.out.println("------------------------------------------");
        System.out.println("Llave publica : " + " [ " + desconocido + "   ]");
        System.out.println("Llave privada : " + " [    " + b + "     ]");
        System.out.println("------------------------------------------");
        System.out.println("");

        System.out.println("----------------------------------");
        System.out.println("[   Ingrese la LLAVE PUBLICA:     ]");
        System.out.println("----------------------------------");
        String cadena = pantalla.nextLine();

        System.out.println("----------------------------------");
        System.out.println("[   Ingrese la LLAVE PRIVADA:     ]");
        System.out.println("----------------------------------");
        int num = pantalla.nextInt();

        System.out.println("");
        System.out.println("");
        char separado2[] = cadena.toCharArray();

        for (int i = 0; i < cadena.length(); i++) {
            separado2[i] = (char) (separado2[i] - (char) (num));

        }
        String desconocido2 = String.valueOf(separado2);
        System.out.println("************************************************");
        System.out.println("");
        System.out.println("Resultado Generado : \n \n [     " + desconocido2 + "     ]");
        System.out.println("");
        System.out.println("************************************************");
    }

}
