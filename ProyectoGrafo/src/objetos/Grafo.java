/**
 *
 * @author Danny
 */
package objetos;

import java.awt.Color;
import java.util.ArrayList;

public class Grafo {

    private ArrayList<Nodo> listaNodo;

    public Grafo() {
        listaNodo = new ArrayList<Nodo>();
    }

    public boolean adjuntarNodo(Nodo nodo) {
        Nodo nodoTemp = buscarNodo(nodo.getDato());
        if (nodoTemp == null) {
            listaNodo.add(nodo);
            return true;
        } else {
            return false;
        }
    }

    public void crearEnlacesDirigidos(Nodo padre, Nodo adyacente, Arista arista) {
        if (padre != null && adyacente != null) {
            padre.addNodoAdyacente(arista, adyacente);
            arista.getLineaQuebrada().setColor(Color.GREEN);
        }
    }

    public void crearEnlacesNoDirigido(Nodo padre, Nodo adyacente, Arista arista) {
        crearEnlacesDirigidos(padre, adyacente, arista);
        crearEnlacesDirigidos(adyacente, padre, arista);
        arista.getLineaQuebrada().setColor(Color.BLACK);
    }

    public Nodo buscarNodo(Object dato) {
        //Objeto nodo
        Nodo temp = null;
        if (dato != null) {
            for (Nodo nodo : listaNodo) {
                if (dato.equals(nodo.getDato())) {
                    temp = nodo;
                }
            }
        }
        return temp;
    }

    public Nodo buscarNodo(int x, int y) {
        Nodo nodoAuxiliar = null;
        for (int i = 0; i < listaNodo.size(); i++) {
            int xNodo = listaNodo.get(i).getCirculo().getX();
            int yNodo = listaNodo.get(i).getCirculo().getY();
            if (x > xNodo && x < (xNodo + listaNodo.get(i).getCirculo().getDiametro())) {
                if (y > yNodo && y < (yNodo + listaNodo.get(i).getCirculo().getDiametro())) {
                    nodoAuxiliar = listaNodo.get(i);
                    break;
                }
            }
        }
        return nodoAuxiliar;
    }

    public ArrayList<Nodo> getAdyacentes(Object dato) {
        ArrayList<Nodo> lista = null;
        Nodo principal = buscarNodo(dato);
        ArrayList<Enlace> aristas = principal.getListaNodoAdyacente();
        if (aristas != null) {
            for (int i = 0; i < aristas.size(); i++) {
                lista.add(aristas.get(i).getNodo());
            }
        }
        return lista;
    }

    public ArrayList<Nodo> getListaNodos() {
        return listaNodo;
    }

    public boolean isAdyacente(Nodo n1, Nodo n2) {
        boolean aux = false;
        ArrayList<Enlace> listaAristas = n1.getListaNodoAdyacente();
        if (listaAristas != null) {
            for (int i = 0; i < listaAristas.size(); i++) {
                if (listaAristas.get(i).getNodo() == n2) {
                    aux = true;
                }
            }
        }
        return aux;
    }

    public boolean isAdyacente(Object datoNodoInicio, Object datoNodoDestino) {
        boolean aux = false;
        Nodo n1 = buscarNodo(datoNodoInicio);
        Nodo n2 = buscarNodo(datoNodoDestino);
        ArrayList<Enlace> listaAristas = n1.getListaNodoAdyacente();
        if (listaAristas != null) {
            for (int i = 0; i < listaAristas.size(); i++) {
                if (listaAristas.get(i).getNodo() == n2) {
                    aux = true;
                }
            }
        }
        return aux;
    }

    
    //Unir los grafos
    public Arista getArista(String nombreVia) {
        Arista aux = null;
        if (nombreVia != null) {
            ArrayList<Nodo> listaN = listaNodo;
            for (Nodo nd : listaN) {
                ArrayList<Enlace> lA = nd.getListaNodoAdyacente();
                for (Enlace enlace : lA) {
                    if (enlace.getArista().getNombreArista().equals(nombreVia)) {
                        aux = enlace.getArista();
                    }
                }
            }
        }
        return aux;
    }

    public Arista getArista(Nodo n1, Nodo n2) {
        Arista aux = null;
        if (isAdyacente(n1, n2)) {
            ArrayList<Enlace> listaAristas = n1.getListaNodoAdyacente();
            for (int i = 0; i < listaAristas.size(); i++) {
                if (listaAristas.get(i).getNodo() == n2) {
                    aux = listaAristas.get(i).getArista();
                }
            }
        } else if (isAdyacente(n2, n1)) {
            aux = getArista(n2, n1);
        }
        return aux;
    }

    public void reiniciarGrafoParaDisjktra() {
        for (Nodo n : listaNodo) {
            n.setMarca(false);
            n.setNodoAntecesorDisjktra(null);
            n.setLongitudCamino(-1);
        }
    }

    
    public void reiniciarColores() {
        if(listaNodo != null){
            for(Nodo nodo: listaNodo){
                nodo.getCirculo().setColor(Color.yellow);
            ArrayList<Enlace> enlazar = nodo.getListaNodoAdyacente();
                if(enlazar != null){
                    for(Enlace enlace:enlazar){
                        if(enlace.getArista().isHabilitado()){ //Si esta unido
                            enlace.getArista().getLineaQuebrada().setGrosorLinea(3);
                        }
                    }
                }
            }
        }        
    }

}
