
package TrabajoArreglosListas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class OperacionesArreglos {

    Scanner leer = new Scanner(System.in);
    private int tamaño;
    private int limiteSuperior;
    private int limiteInferior;
    private int arreglo[];
   
    public void pedirValores (){
        System.out.print("\nTamaño: ");
        int tamaño2 = leer.nextInt();
        this.tamaño=tamaño2;
        
        System.out.print("Limite Superior: ");
        int limSuperior = leer.nextInt();
        this.limiteSuperior = limSuperior;
        
        System.out.print("Limite Inferior: ");
        int limInferior = leer.nextInt();
        this.limiteInferior = limInferior;
    }
    
    public int[] crearArreglo(int tamaño, int limiteSuperior, int limiteInferior){
    
        int arreglo2[] = new int [tamaño];
        
        for (int i = 0; i< tamaño; i++){
            arreglo2[i] = (int)(Math.random()*(limiteSuperior - limiteInferior) + limiteInferior);
        }
        this.arreglo = arreglo2;
        return arreglo2;
        
    }
    
    
    public void imprimirArreglo(int []arreglo){
        for(int i = 0; i < arreglo.length;i++){
            System.out.print(arreglo[i]+ " ");
        }
    }
    public void imprimirArregloLong(long []arreglo){
        for(int i = 0; i < arreglo.length;i++){
            System.out.print(arreglo[i]+ " ");
        }
    }
    
    public void imprimirArregloPosiones (int []arreglo2){
        for (int i = 0; i < arreglo2.length;i++){
            System.out.println("Posicion "+i+ " --> Valor: "+arreglo[i]);
        }
    }
    
    public int[][] crearMatriz (int tamaño, int limiteSuperior, int limiteInferior){
        
        int matriz[][] = new int [tamaño][tamaño];
        
        for(int i = 0; i<matriz.length; i++){
            for(int j = 0; j< matriz.length;j++){
                matriz[i][j] = (int)(Math.random()*(limiteSuperior - limiteInferior) + limiteInferior);
            }
            
        }
        return matriz;
    }
    
    public void imprimirMatriz(int [][]matriz){
        for(int i = 0; i<matriz.length; i++){
            for(int j = 0; j< matriz.length;j++){
                System.out.print(matriz[i][j]+" ");
            }
            System.out.println("");
        }
    }
    
    public List<Integer> generarLista(int tamaño, int limiteSuperior, int limiteInferior){
        List<Integer> listapedida = new ArrayList<Integer>();
        
        int valor;
       
        
        for(int i = 0; i<tamaño; i++){
            do{
                 valor = (int)(Math.random()*(limiteSuperior - limiteInferior) + limiteInferior);
            }while(listapedida.contains(valor));
            listapedida.add(valor);
}
        
        return listapedida;
        
    }
    public void listaOrdenada(List lista){
        imprimirLista(lista);
        /*ordena los valores de mayor a menor se utiliza
          Comparator<Integer> comparador = Collections.reverseOrder();
          se compara con Collections(lista,comparador);
        */
        Comparator<Integer> comparador = Collections.reverseOrder();
        
        
        for(int i = 0; i < tamaño;i++){
            Collections.sort(lista , comparador);
        }
        
        System.out.println("\nLa lista ordenada es: ");
        imprimirLista(lista);
       
    }
    
    public void elementosComunes(int[][]arreglo1, int[][]arreglo2){
        System.out.print("Los numeros comunes son: ");
        for(int i = 0; i < arreglo1.length;i++){
            for (int j = 0; j < arreglo2.length;j++){
                if (i==j) {
                    System.out.print(arreglo1[i][j] + " " );
                 }
                
            }
        }
    }
    public void diccionario (){
        
        HashMap<String,String> diccionario = new HashMap<String,String>();
        diccionario.put("Enero",        " January");
        diccionario.put("Febrero",      " February");
        diccionario.put("Marso",        " March");
        diccionario.put("Abril",        " April");
        diccionario.put("Mayo",         " May");
        diccionario.put("Junio",        " June");
        diccionario.put("Julio",        " July");
        diccionario.put("Agosto",       " Augus");
        diccionario.put("Septiembre",   " Sptember");
        diccionario.put("Octubre",      " October");
        diccionario.put("Noviembre",    " Novenber");
        diccionario.put("Diciembre",    " Dicember");
        System.out.println("Ingrese el mes ");
            
        String mes = leer.nextLine();
        for(Map.Entry<String, String>entry:diccionario.entrySet()){
            if (mes.equals(entry.getKey())) {
            System.out.println( "La traduccion del mes que ingreso es "+entry.getValue());  
        }
            
        } 
        
    }
    public void abecedario(){
        char []arregloz = new char [25];
        int valor;
        for(int i = 0; i < arregloz.length; i++){
            
            valor = (int)(Math.random()*(90 - 65) + 65);
            char num = (char)valor;
            arregloz[i] = num;
            System.out.print(arregloz[i]+" ");
        }
        
    }
    public void numMes(){
        System.out.print("Digite un numero del 1 al 12: ");
        int valor;
        do{
            valor = leer.nextInt();
            if(valor<0 || valor>12){
                System.out.print("Digite un valor correcto: ");
            }
        }while(valor<0|| valor>12);
        
        
        switch(valor){
            case(1):System.out.print("El "+valor+" corresponde al mes de ENERO");
            break;
            case(2):System.out.print("El "+valor+" corresponde al mes de FEBRERO");
            break;
            case(3):System.out.print("El "+valor+" corresponde al mes de MARZO");
            break;
            case(4):System.out.print("El "+valor+" corresponde al mes de ABRIL");
            break;
            case(5):System.out.print("El "+valor+" corresponde al mes de MAYO");  
            break;
            case(6):System.out.print("El "+valor+" corresponde al mes de JUNIO");    
            break;
            case(7):System.out.print("El "+valor+" corresponde al mes de JULIO");    
            break;
            case(8):System.out.print("El "+valor+" corresponde al mes de AGOSTO");    
            break;
            case(9):System.out.print("El "+valor+" corresponde al mes de SEPTIEMBRE");
            break;
            case(10):System.out.print("El "+valor+" corresponde al mes de OCTUBRE");
            break;
            case(11):System.out.print("El "+valor+" corresponde al mes de NOVIEMBRE");
            break;
            case(12):System.out.print("El "+valor+" corresponde al mes de DICIEMBRE");
            break;
        }
       
         
    }
    public void conversorTemperatura(){
        int numero = 0;
        System.out.println("1. De Centrigrados a Fahrenheit "
                + "\n2. De Fahrenheit  a Centigrados");
        System.out.print("Digite la opcion que desea convertir: ");
        while (numero <1 || numero >2){
            numero = leer.nextInt();
            if(numero <1 || numero >2){
                System.out.print("Digite un valor válido: ");
            }
        }
        double valor;
        if (numero == 1){
            System.out.print("Digite el valor a convertir: ");
            valor = leer.nextInt();
            
            double fahrenheit =((9.0/5.0)*(valor))+32;
            System.out.print(valor+"°C en Fahrenheit son: "+fahrenheit+"°F\n\n");
        }
        if (numero == 2){
            System.out.print("Digite el valor a convertir: ");
            valor = leer.nextInt();
            double centigrados =((5.0/9.0)*(valor))+32;
            
            System.out.print(valor+"°F en Centígrados son: "+centigrados+"°C\n\n");
        }
    }
    
    public void segundoHoras (){
        System.out.print("Digite los segundos a convertir: ");
        long valor;
        do{
            valor = leer.nextLong();
            if (valor < 0 || valor >2147483647){
                System.out.print("Digite valores positivos: ");
            }
        }while(valor< 0 || valor >2147483647);
        
    
        long hors =  valor/3600;
        long min = (valor- (hors*3600))/60;
        long seg = (valor - ((hors*3600)+(min*60)));
        System.out.println("\nHH:MM:SS");
        System.out.println(hors+" : "+min+" : "+seg);
        
     }
    
    public void tablaMultiplicar(){
        System.out.print("Digite un numero para imprimir la tabla del 1 al 10 multiplicado con dicho numero: ");
        int n = leer.nextInt();
        for(int i = 1; i <= 10; i++){
            System.out.println(n+" x "+i+ " = "+(n*i));
        }
    }
    
    public void numImpares(){
       System.out.print("Cuantas filas de numeros impares desea obtener: ");
        int filas;
        do{
            filas = leer.nextInt();
            if(filas<0){
                System.out.print("Digite un valor positivo: ");
            }
        }while(filas<0);
        int impar = 1;
        int valor = 0;
        
        for(int i = 0; i<filas; i++){
            for(int j = 0; j<=valor ;j++){
                System.out.print(impar+ " ");
                impar= impar+2;
            }
            valor++;
            impar= 1;
            
            System.out.println("");
        }
    }
    
    public void serieCompleja (){
        long suma = 0;
        int valorx;
        System.out.print("Digite cuantas veces quiere que se imprima la serie: ");
        do{
            valorx = leer.nextInt();
            if(valorx<0){
                System.out.print("Digite un valor positivo: ");
            }
        }while (valorx<0);
        
        int valor2 = (valorx * 3);
        
        long [] arreglo3 = new long [valor2]; 
        long aux3 =4;
        
        for(int i = 0; i < arreglo3.length; i++){
            
            arreglo3[i] = aux3-2;
            long aux = arreglo3[i];
            i++;
            
            arreglo3[i]=(int)(Math.pow(aux, 2));
            long aux2 = arreglo3[i];
            i++;
            
            arreglo3[i] =(aux2*2);
            aux3 = arreglo3[i];
            
            suma = suma + aux + aux2 + aux3;
        }
        
        imprimirArregloLong(arreglo3);
        System.out.println("\nLa suma  de los terminos es: "+suma);
    }
    
    public void serie (){
        System.out.print("Digite cuantos numeros quiere imprimir en la serie: ");
        int num; 
        do{
            num = leer.nextInt();
            if(num<0){
                System.out.print("Digite un valor positivo: ");
            }
        }while(num<0);
        int valor1 = 1;
        int valor2 =-1;
        
        for (int i = 1; i <= num; i++) {
            System.out.print((i*valor1)+ ", "+(i*valor2)+", ");
        }

        
    }
    
    public void secuencia (){
        System.out.print("Cuantas filas desea obtener: ");
        int valor; 
        do{
            valor = leer.nextInt();
            if(valor<0){
                System.out.print("Digite un valor positivo: ");
            }
        }while(valor<0);
        int aux = valor;
        int valor1 = valor;
        
        System.out.println("");
       for(int i = 0; i<valor; i++){
            for(int j = 0; j<valor1 ;j++){
                System.out.print(aux+ " ");
            }
            valor1--;
            aux--;
            System.out.println("");
        }
    }
    
    public void imprimirLista (List lista){
        for(Object o: lista){
            System.out.print(o+ " ");
        }
    }
    
     
     public void arregloFibonacci(int x){
         int []fibonacci = new int[x]; 
         int a =0;
         int b =1;
         int fib;
         for (int i = 0; i < x-1; i++){
            fib = a + b;
            fibonacci[i] = fib;
            a = b;
            b = fib;
            if(i==0){
                System.out.print("1, ");
            }
            System.out.print(+fib+ ", ");  
        }
         
          
     }
     
    public void maximoMinimoLista(List listaz){
        List<Integer> lista = listaz;
       
        int max=0;
        int min;
        double media=0;
        for(int i = 0; i < lista.size();i++){
            if (lista.get(i)> max) {
                max=lista.get(i);
            } 
        }
        min = max;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i)< min) {
               min=lista.get(i);
            }
        }
        for (int i = 0; i < lista.size(); i++) {
           media=media+lista.get(i);
        } 
        media = media/lista.size();
        
        System.out.println("\nEL valor maximo es: "+max);
        System.out.println("EL valor minimo es: "+min);
        System.out.println("La media aritmética de la lista es: "+media);
    }

   
    public void adivinaNum (){
        
        System.out.println("Estoy pensado en un número del 1 al 50");
        System.out.println("Tiene 5 Intentos.\nDigite un número\nPrimer Intento ");
        int num;
        do {
            num = leer.nextInt();
            if (num < 0 || num > 50){
                System.out.println("Tiene que ser del 1 al 50. Inserte nuevamente: ");
            }
        }while (num < 0 || num > 50);
        
        int valor = (int) (Math.random()* (50-1)+1);
        System.out.println("Valor es:" +valor);
        
        
        for (int i = 0; i < 5; i++) {
            if (i == 0){
                
                if(valor == num){
                    System.out.println("“Felicidades, adivinaste el numero”");
                    break;
                }
                else{
                    
                    if(valor > 10 && valor<50 && num >10){ 
                        System.out.print("El numero esta entre "+(num-10)+" y 50. Inserte otro numero: ");
                        num = leer.nextInt();
                    
                    }
                    else {
                        System.out.println("El numero esta entre "+(num)+" y 50. Inserte otro numero: ");
                        num = leer.nextInt();
                        
                                
                    }
                }
                
            }    
            if(i == 1){
                System.out.println("Segundo intento");
                if (valor == num){
                    System.out.println("“Felicidades, adivinaste el numero”");
                    break;
                }
                else{
                    
                    if(valor > 10 && valor<50 && num >15){
                        System.out.print("El numero esta entre "+(num-4)+" y 50. Inserte otro numero: ");
                        num = leer.nextInt();
                    }
                    else{
                        System.out.print("El numero esta entre "+(num)+" y 50. Inserte otro numero: ");
                        num = leer.nextInt();
                    }
                } 
            }
            
            if (i == 2){
                System.out.println("Tercer intento");
                if(valor == num){
                    System.out.println("“Felicidades, adivinaste el numero”");
                    break;
                }
                else{
                    if(valor > 10 && valor<40 && num >25 && num<30){
                        System.out.print("El numero esta entre "+(num-3)+" y 45. Inserte otro numero: ");
                        num = leer.nextInt();
                    }
                    else{
                        System.out.println("El numero esta entre "+num+" y "+valor+". Inserte otro numero: ");
                        num = leer.nextInt();
                    }
                }
            }
            
            if(i == 3){
                System.out.println("Cuarto intento");
                if(valor == num){
                    System.out.println("“Felicidades, adivinaste el numero”");
                    break;
                }
                else{
                    if(valor > 15 && valor<30 && num <20){
                        System.out.print("El numero esta entre "+(num-2)+" y 30. Inserte otro numero: ");
                        num = leer.nextInt();
                        
                    }
                    else{
                        System.out.println("El numero esta entre "+num+" y "+valor+". Inserte otro numero: ");
                        num = leer.nextInt();
                        
                    }
                }
            }    
            if (i == 4){
                System.out.println("Quinto intento"); 
                if(valor == num){
                    System.out.println("“Felicidades, adivinaste el numero”");
                    break;
                }
                else{
                    System.out.println("“Se acabaron los intentos, el numero que pensé era: "+valor );
                }
            }
        }
        
    }    
    
    public int cuboPerfecto(int valor){
        int num= valor;
        int verificador;
        int suma=0;
       
        while(valor != 0){
            int res = (valor%10); //Ultimo digito
            suma = (int) (suma + (Math.pow(res, 3))); //ultimo digito elevado a la potencia de diez y suma
            valor = valor /10; //Elimina el ultimo digito
        }
        System.out.println("La suma de las potencias es: "+suma);
        if (suma == num){
            verificador = 1;
        }
        else{
            verificador =0;
        }
        return verificador;
    }
    
    public void matrizTranspuesta(){
        System.out.print("Digite el tamaño que quiere para su matriz: ");
        int tam = leer.nextInt();
        int matrizx[][] = new int [tam][tam];
        System.out.println("Matriz original");
        for(int i = 0; i<matrizx.length; i++){
            for(int j = 0; j< matrizx.length;j++){
                matrizx[i][j] = (int)(Math.random()*(100));
            }
        }
        
        imprimirMatriz(matrizx);
        System.out.println("\nLa matriz transpuesta es: \n");
        
        for(int k = 0; k<matrizx.length; k++){
            for(int m = 0; m< matrizx.length;m++){
                System.out.print(matrizx[m][k]+" ");
            }
            System.out.println("");
        }
        
        
        
    }

    public int getTamaño() {
        
        return tamaño;
    }
    

    public int getLimiteSuperior() {
        return limiteSuperior;
    }

    public int getLimiteInferior() {
        return limiteInferior;
    }

    public int[] getArreglo() {
        return arreglo;
    }

    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }
    
    
}
