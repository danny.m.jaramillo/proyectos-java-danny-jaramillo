
package TrabajoArreglosListas;

import java.util.List;
import java.util.Scanner;


public class Menu {
    //programcion avansada
    public static void main (String[] args){
        OperacionesArreglos q= new OperacionesArreglos();
        int opcion = 0;
        Scanner leer = new Scanner(System.in);
        do{
            System.out.println("-----------------------------------------------------------------------\n"
                    +"Escoja una opcion\n" + 
                    "------------------------------------------------------------------------"
                    +"\n1. Generar arreglo/matriz/lista." 
                    +"\n2. Generar arreglo con posiciones incluidas."
                    +"\n3. Números fibonacci."
                    +"\n4. Lista con números al azar y ordenar esa lista de mayor a menor."
                    +"\n5. Maximo, minimo, media de una lista"
                    +"\n6. Encontrar elementos comunes de 2 matrices."
                    +"\n7. Arreglo con letras del abecedario."
                    +"\n8. Da un número para saber el mes corresponde a dicho numero"
                    +"\n9. "
                    +"\n10. Conversor de Temperatura."
                    +"\n12. Convertir segundos en formato HH:MM:SS"
                    +"\n13. Tabla de multiplicar con valor de n."
                    +"\n14. Presentar numero impares en forma de triangulo rectangulo."
                    +"\n15. Sumatoria Serie Compleja"
                    +"\n16. Serie con signos cambiados hasta n valor."
                    +"\n17. Adivina el numero."
                    +"\n18. Secuencia triangular"
                    +"\n19. Comprobar si un numero es cubo perfecto"
                    +"\n20. Obtener matriz transpuesta"
                    +"\n0. Salir del programa.\n"
                    +"------------------------------------------------------------------------\n");
            try{
                System.out.print("Digite un valor: ");
                opcion = leer.nextInt();
                if (opcion == 1){
                    System.out.print("Que matriz desea generar: "
                            + " 1. Arreglo Unimensional."
                            + " 2. Matriz. "
                            + " 3. Lista."
                            + " 4. Salir.\n");
                            int valor = 0;
                            do{
                                if(valor<0 || valor >4){
                                    System.out.print("Digite un valor valido: ");
                                }
                                valor = leer.nextInt();
                            }while(valor<0 || valor >4);
                            
                            if (valor == 1 ){
                                System.out.println("Digite los valores para su Arreglo Unimensional. ");
                                q.pedirValores();
                                int [] arreglo3 = (q.crearArreglo(q.getTamaño(), q.getLimiteSuperior(), q.getLimiteInferior()));
                                q.imprimirArreglo(arreglo3);
                                break;
                                                               
                            }
                            if (valor == 2 ){
                                System.out.println("Digite los valores para su Matriz. ");
                                q.pedirValores();
                                int [][] matriz3 = (q.crearMatriz(q.getTamaño(), q.getLimiteSuperior(), q.getLimiteInferior()));
                                q.imprimirMatriz(matriz3);
                                break;
                            }
                            if (valor == 3 ){
                                System.out.println("Digite los valores para su Lista. ");
                                q.pedirValores();
                                List<Integer> lista3 = (q.generarLista(q.getTamaño(), q.getLimiteSuperior(), q.getLimiteInferior()));
                                q.imprimirLista(lista3);
                                break;
                            }
                            if (valor == 4){
                                break;
                            }
                }
                else if (opcion == 2){
                    System.out.print("Digite el TAMAÑO para su Arreglo Unimensional con valores azar del 10 al 30: ");
                                int tamaño5; 
                                do{
                                    tamaño5= leer.nextInt();
                                    if(tamaño5<0){
                                        System.out.print("Digite un valor positivo: ");
                                    }
                                }while(tamaño5<0);
                                int [] arreglo3 = (q.crearArreglo(tamaño5,30, 10));
                                q.imprimirArregloPosiones(arreglo3);
                                break;
                }
                else if (opcion == 3){
                    System.out.print("Digite el Tamaño: ");
                    int x;
                    do{
                        x= leer.nextInt();
                        if(x<0){
                            System.out.print("Digite un valor positivo: ");
                        }
                    }while(x<0);
                    q.arregloFibonacci(x);
                    break;
                }
                else if (opcion == 4){
                    System.out.println("Digite los valores para su Lista. ");
                    q.pedirValores();
                    System.out.print("La lista desordenada es: ");
                    List<Integer> lista6 = (q.generarLista(q.getTamaño(), q.getLimiteSuperior(), q.getLimiteInferior()));
                    q.listaOrdenada(lista6);
                    break;
                }    
 
                    
                
                else if (opcion == 5){
                    System.out.println("\nPara la lista digite");
                    q.pedirValores();
                    List<Integer>lista3 = q.generarLista(q.getTamaño(), q.getLimiteSuperior(), q.getLimiteInferior());
                    System.out.print("\nSu lista  de numeros es: ");
                    q.imprimirLista(lista3);
                    
                    q.maximoMinimoLista(lista3);
                    break;
                }
                else if (opcion == 6){
                    System.out.println("El rango de los valores tiene que ser corto para encontrar coincidencias.\nDigite los valores para su Matriz. ");
                    q.pedirValores();
                    int [][] arreglo1 = (q.crearMatriz(q.getTamaño(), q.getLimiteSuperior(), q.getLimiteInferior()));
                    int [][] arreglo2 = (q.crearMatriz(q.getTamaño(), q.getLimiteSuperior(), q.getLimiteInferior()));
                    q.imprimirMatriz(arreglo1);
                    q.imprimirMatriz(arreglo2);
                    q.elementosComunes(arreglo1, arreglo2);
                }
                else if (opcion == 7){
                    q.abecedario();
                    break;
                }
                else if (opcion == 8){
                    q.numMes();
                    break;
                }
                else if (opcion == 9){
                    q.diccionario();
                    break;
                }
                else if (opcion == 10){
                    q.conversorTemperatura();
                    break;
                }
                else if (opcion == 12){
                    q.segundoHoras();
                    break;
                }
                else if (opcion == 13){
                    q.tablaMultiplicar();
                    break;
                }
                else if (opcion == 14){
                    q.numImpares();
                    break;
                }
                else if (opcion == 15){
                    q.serieCompleja();
                    break;
                }
                else if (opcion == 16){
                    q.serie();
                    break;
                }
                else if (opcion == 17){
                    q.adivinaNum();
                    break;
                }
                else if (opcion == 18){
                    q.secuencia();
                    break;
                }
                else if (opcion == 19){
                    System.out.print("Digite el valor que desea comprobar: ");
                    int valor = leer.nextInt();
                    int verif = (q.cuboPerfecto(valor));
                    if(verif ==1){
                        System.out.println("El numero "+valor+ " SI es un cubo perfecto.");
                    }
                    if(verif ==0){
                        System.out.println("El numero "+valor+ " NO es un cubo perfecto.");
                    }
                    break;
                }
                else if (opcion == 20){
                    q.matrizTranspuesta();
                    break;
                }
                
                
            }catch(Exception e ){
                System.out.println("Ingrese un valor valido");
            }
        }while(opcion != 0);
        System.out.println("");
    }    
}
