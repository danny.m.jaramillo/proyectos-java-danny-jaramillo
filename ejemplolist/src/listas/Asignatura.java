/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class Asignatura {

    /**
     * @return the creditos
     */
    Scanner lector = new Scanner(System.in);
    private String nombre;
    private int creditos;
    private int nrHoras;

    public Asignatura(String nombre ,int credito , int nrohoras) {
        this.nombre=nombre;
        this.nrHoras=nrohoras;
        this.creditos=credito;

    }

    public int getCreditos() {
        return creditos;
    }

    /**
     * @param creditos the creditos to set
     */
    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    /**
     * @return the nrHoras
     */
    public int getNrHoras() {
        return nrHoras;
    }

    /**
     * @param nrHoras the nrHoras to set
     */
    public void setNrHoras(int nrHoras) {
        this.nrHoras = nrHoras;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
