/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class metodos {

    public static void main(String[] args) {

        List<String> lista1 = new ArrayList<String>();
        lista1.add("juan");
        lista1.add("123");
        lista1.add("1223");
        System.out.println(" " + lista1);

        // cambiar elemento 
        lista1.set(0, "danny");
        System.out.println(" " + lista1);
        // elimar elemento  conciendo el valor
        System.out.println("--------------------");
        lista1.remove("danny");
        System.out.println(" " + lista1);
        System.out.println("--------------------");
        // elimar elemento con posicion
        lista1.remove(0);
        System.out.println(" " + lista1);

    }
}
