/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class Estudiante {
    
    private String cedula;
    private String nombre;
    private int edad;
    List<Asignatura> listadoAsiganturas = new ArrayList<Asignatura>();

    //constructor
    /**
     * Constructor
     */
    public Estudiante() {
        
    }
    // contructor 

    public Estudiante(String cedula, String n, int e) {
        this.cedula = cedula;
        this.nombre = n;
        this.edad = e;
        
    }
    
    public List<Asignatura> matricular(String nombre, Asignatura asignatura) {
        this.listadoAsiganturas.add(asignatura);
      
        
        return listadoAsiganturas;
        
    }
//ver las asiganturas  o ver materias matriculadas 

    /**
     * Ver matricula dddd
     * @param listaMaterias 
     */
    public void verMatricular(List<Asignatura> listaMaterias) {
        
        for (Asignatura elemento : listaMaterias) {
            System.out.println(elemento.getNombre());
        }

        //landa 
        System.out.println(" con exprecion . LANDA..");
        listaMaterias.forEach((Asignatura elemento) -> System.out.println(elemento.getNombre()));
        
    }
    
    public void elimnarAsignaturas(List<Asignatura> listaMaterias) {
        listaMaterias.remove(0);
        this.verMatricular(listaMaterias);
    }
    
    public void tomarCurso() {
        
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }
    
}
