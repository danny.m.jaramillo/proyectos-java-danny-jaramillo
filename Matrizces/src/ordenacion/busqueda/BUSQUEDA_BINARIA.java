/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenacion.busqueda;

import java.util.Scanner;

/**
 *
 * @author mac
 */
public class BUSQUEDA_BINARIA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner ln = new Scanner(System.in);
        BUSQUEDA_BINARIA bn = new BUSQUEDA_BINARIA();
        System.out.println("Ingrese tamaño del arreglo:");
        int tamaño = ln.nextInt();
        System.out.println("Ingrese valores del arreglo:");
        int arreglo[] = new int[tamaño];
        for (int i = 0; i < arreglo.length; i++) {
            System.out.print(" Ingrese un valor par la posicion  " + i + "  = ");
            arreglo[i] = ln.nextInt();
        }

        System.out.println("Los valores del arreglo son ( ordenados) : ");
        bn.burbuja(arreglo);
        for (int i = 0; i < arreglo.length; i++) {
            System.out.print(arreglo[i] + " ");
        }

        System.out.println("\n");
        System.out.println(" Ingrese valor a buscar:");
        System.out.println(" ");
        int busca = ln.nextInt();
        long inicio = System.nanoTime();//controla el tiempo
        System.out.println(bn.binario(busca, arreglo));

        long fina = System.nanoTime(); // para controlar el tiempo

        double tiempo = (double) (fina - inicio) * 1.0e-9;
        System.out.println("Tiempo de ejecucion " + tiempo + " nanosegundos");
    }

    //ordenacion burbuja
    public void burbuja(int[] arreglo) {
        try {
            int aux;
            boolean orden = false;
            int cont = 1;

            while (true) {

                orden = false;
                for (int i = 1; i < arreglo.length; i++) {
                    if (arreglo[i] < arreglo[i - 1]) {
                        aux = arreglo[i];
                        arreglo[i] = arreglo[i - 1];
                        arreglo[i - 1] = aux;
                        orden = true;
                    }

                }
                if (orden == false) {
                    break;
                }
                System.out.println(" ");

            }
        } catch (Exception e) {
            System.out.println("error");
        }

    }

    //busqueda binaria
    public String binario(int valor, int[] arreglo) {
        int medio;
        int fin = arreglo.length - 1;
        int inicio = 0;
        int pasada = 1;
        //operacion de busqueda
        while (inicio <= fin) {

            System.out.println("\n pasada " + pasada++);
            System.out.println("valor inferior :  " + arreglo[inicio]);
            System.out.println("valor limite:  " + arreglo[fin]);
            for (int j = inicio; j <= fin; j++) {
                System.out.print(arreglo[j] + " ");
            }
            System.out.println("");
            // dicidir el arreglo
            medio = (inicio + fin) / 2;
            if (arreglo[medio] == valor) {
                System.out.println("El valor buscado se encuentra en al posicion");
                return medio + "";
            } else {
                //suma una posicion al incio
                if (arreglo[medio] < valor) {
                    inicio = medio + 1;
                } else {
                    //resta una posicion al final
                    fin = medio - 1;
                }
            }

        }
        System.out.println(" ");
        return "No se encuentra el valor en el vector";

    }
}
