/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenacion.busqueda;

import java.util.Scanner;

/**
 *
 * @author ALEXIS
 */
public class BUSQUEDA_SECUENCIAL2 {

    public static void main(String[] args) {

        long TInicio, TFin;
        long tiempo;
        double tiempo1;
        Scanner leer = new Scanner(System.in);
        int tamaño;
        int dato;
        boolean condicion = false;
        int contador = 1;
        int controlador = 0;

        System.out.println("Digite el tamaño del arreglo:");
        tamaño = leer.nextInt();

        int[] arreglo = new int[tamaño];

        for (int i = 0; i < tamaño; i++) {
            arreglo[i] = (int) (Math.random() * 20);
            System.out.print(arreglo[i] + " ");

        }
        System.out.println("");

        System.out.println("Digite el numero a buscar");
        dato = leer.nextInt();

        //busqueda secuencial
        int i = 0;
        TInicio = System.nanoTime();
        while (i < tamaño) {
            condicion = false;
            while (condicion == false) {
                System.out.println("Comparación " + contador + ": ");
                System.out.println(arreglo[i] + " = " + dato);

                if (arreglo[i] == dato) {
                    condicion = true;
                    controlador++;
                } else {
                    System.out.println("No es igual.");
                }
                contador++;
                i++;
            }
        }
        TFin = System.nanoTime();

        if (condicion == false) {
            System.out.println("No se ha encontrado el numero ha buscar");
        } else {
            System.out.println("El número ha sido encontrado " + controlador + " veces.");
//            for (int j = 0; j < tamaño; j++) {
//                if(arreglo[i] == dato){
//                System.out.println("En la posicion: "+(i));
                }
            

        tiempo = TFin - TInicio;
        System.out.println("Tiempo de ejecución en nanosegundos: " + tiempo);
        tiempo1 = tiempo * (1.0 * 10e-9);
        System.out.println("Tiempo de ejecución en segundos: " + (tiempo1));

    }

}
