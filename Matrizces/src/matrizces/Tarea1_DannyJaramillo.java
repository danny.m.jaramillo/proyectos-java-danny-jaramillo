 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrizces;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class Tarea1_DannyJaramillo {
// Danny Michael Jaramillo Jumbo

    public static void main(String[] args) {
        boolean opcion = true;
        System.out.println("************");
        System.out.println("*BIENVENIDO*");
        System.out.println("************");

        do {
            Scanner lector = new Scanner(System.in);
            System.out.println("");
            System.out.println(" Escoja una opcion.");
            System.out.println(" ------------------");
            System.out.println("[ 1 ] Calcular la determinante.");
            System.out.println("[ 2 ] Tranformar Numeros Arabico a Numeros Romanos.");
            int o = lector.nextInt();

            if (o == 1) {
                int valor1;
                int valor2;
                int valor3;
                int determinante;

                int[][] a = new int[3][3];
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        a[i][j] = (int) Math.floor(Math.random() * (100 - 1)) + 1;

                    }

                }
                System.out.println(" La matris de 3x3 generada con numeros aleatorios es la siguiente:");
                System.out.println(" ------------------------------------------------------------------");
                System.out.println(" ");

                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {

                        System.out.print("  " + a[i][j] + "   ");
                    }
                    System.out.println("");

                }
                System.out.println("");
                System.out.println("------------------------------------------------------------------");
                System.out.println(" Calcular su Determinante");
                System.out.println(" 1. Se aplico el principio de los cofactores ");
                System.out.println(" 2.Los cofactores generados fueron:");
                System.out.println("------------------------------------------------------------------");

                valor1 = a[0][0] * ((a[1][1] * a[2][2]) - (a[2][1] * a[1][2]));
                valor2 = a[0][1] * ((a[1][0] * a[2][2]) - (a[2][0] * a[1][2]));
                valor3 = a[0][2] * ((a[1][0] * a[2][1]) - (a[2][0] * a[1][1]));
                determinante = valor1 - valor2 + valor3;

                System.out.println("");
                System.out.println(" (1)  " + valor1);
                System.out.println(" (2)  " + valor2);
                System.out.println(" (3)  " + valor3);
                System.out.println("");
                System.out.println(" La determinante de la matris de 3x3 es :");
                System.out.println(" (" + valor1 + ") - (" + valor2 + ") + (" + valor3 + ")  = " + determinante);
                System.out.println("");
                System.out.println("Determinante =  " + determinante + "  R//");

            } else if (o == 2) {

                //Transformacion
                //M :1000
                //D :500
                //C:100
                //L:50
                //X:10
                //V:5
                //I:1
                // el numero mas grande que se pude representar con estos simbolos es el 3999
                int numero, unidades, decenas, centenas, millar, millon;
                System.out.println("------------------");
                System.out.println(" Digite un Numero");
                System.out.println("------------------");
                System.out.println("Dato importante:");
                System.out.println("El maximo numero que se puede representar es el 9999.");
                numero = lector.nextInt();
                if( numero <=9999){
                millon = numero / 1000;
                unidades = numero % 10;
                numero = numero / 10;
                decenas = numero % 10;
                numero = numero / 10;
                centenas = numero % 10;
                numero = numero / 10;
                millar = numero % 10;
                numero = numero / 10;

                System.out.println("");
                switch (millon) {

                    case 4:
                        System.out.println(" __");
                        System.out.print(" [IV]");
                        break;
                    case 5:
                        System.out.println(" __");
                        System.out.print("[V]");
                        break;
                    case 6:
                        System.out.println(" __");
                        System.out.print("[VI]");
                        break;
                    case 7:
                        System.out.println(" ___");
                        System.out.print(" VII");
                        break;
                    case 8:
                        System.out.println(" ____");
                        System.out.print(" VIII");
                        break;
                    case 9:
                        System.out.println(" __");
                        System.out.print(" IX");
                        break;

                }
                switch (millar) {
                    case 1:
                        System.out.print("M ");
                        break;
                    case 2:
                        System.out.print("MM ");
                        break;
                    case 3:
                        System.out.print("MMM");
                        break;

                }
                switch (centenas) {
                    case 1:
                        System.out.print("C");
                        break;
                    case 2:
                        System.out.print("CC");
                        break;
                    case 3:
                        System.out.print("CCC");
                        break;
                    case 4:
                        System.out.print("CD");
                        break;
                    case 5:
                        System.out.print("D");
                        break;
                    case 6:
                        System.out.print("DC");
                        break;
                    case 7:
                        System.out.print("DCC");
                        break;
                    case 8:
                        System.out.print("DCCC");
                        break;
                    case 9:
                        System.out.print("CM");
                        break;

                }
                switch (decenas) {
                    case 1:
                        System.out.print("X");
                        break;
                    case 2:
                        System.out.print("XX");
                        break;
                    case 3:
                        System.out.print("XXX");
                        break;
                    case 4:
                        System.out.print("XL");
                        break;
                    case 5:
                        System.out.print("L");
                        break;
                    case 6:
                        System.out.print("LX");
                        break;
                    case 7:
                        System.out.print("LXX");
                        break;
                    case 8:
                        System.out.print("LXXX");
                        break;
                    case 9:
                        System.out.print("XC");
                        break;

                }
                switch (unidades) {
                    case 1:
                        System.out.print("I");
                        break;
                    case 2:
                        System.out.print("II");
                        break;
                    case 3:
                        System.out.print("III");
                        break;
                    case 4:
                        System.out.print("IV");
                        break;
                    case 5:
                        System.out.print("V");
                        break;
                    case 6:
                        System.out.print("VI");
                        break;
                    case 7:
                        System.out.print("VII");
                        break;
                    case 8:
                        System.out.print("VIII");
                        break;
                    case 9:
                        System.out.print("IX");
                        break;

                }
                System.out.println("");
                } else { 
                    System.out.println(" Ingreso un numero fuera de rango (9999)");
                }

            } else {
                System.out.println("");
                System.out.println(" Ingreso un numero incorrecto ");
                System.out.println("");
            }
            System.out.println("");
            System.out.println("Decea realizar otra operacion");
            System.out.println("[ 1 ] si");
            System.out.println("[ 2 ] no");
            int p = lector.nextInt();

            if (p == 1) {
                opcion = true;
            } else if (p == 2) {
                opcion = false;
                System.out.println("*************************");
                System.out.println("! Gracias por su visita !");
                System.out.println("*************************");
            } else {
                System.out.println(" Ingreso un Numero incorrecto");
            }

        } while (opcion);
    }

    public void f(int x) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
