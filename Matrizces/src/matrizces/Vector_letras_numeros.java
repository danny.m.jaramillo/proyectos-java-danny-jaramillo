/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrizces;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author DUsuario
 */
public class Vector_letras_numeros {

    // Determino si es entero o letra
    public static boolean isNumeric(String cadena) {

        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }

    public static void main(String[] args) {

        long TInicio, TFin;
        long tiempo;
        double tiempo1;
        Scanner leer = new Scanner(System.in);
        int tamaño;
        String dato;
        boolean condicion = false;

        System.out.println("Digite el tamaño del arreglo:");
        tamaño = leer.nextInt();
        System.out.println("------------------------------");

        String[] arreglo = new String[tamaño];

        for (int i = 0; i < tamaño; i++) {
            System.out.println("Ingrese un numero o letra :");
            String ap = leer.next();
            arreglo[i] = ap.toLowerCase();

        }
        System.out.println("");

        System.out.println(" ");
        System.out.println(" ");
        //busqueda secuencial
        int i = 0;
        TInicio = System.nanoTime();
        List<Integer> lista = new ArrayList<Integer>();
        List<String> lista1 = new ArrayList<String>();
        while (i < tamaño) {

            int numero;

            if (isNumeric(arreglo[i]) == true) {
                numero = Integer.parseInt(arreglo[i]);

                lista.add(numero);
            } else {

                lista1.add(arreglo[i]);
            }

            i++;

        }

        String[] arre = new String[lista1.size()];
        Integer[] arreqq = new Integer[lista.size()];

        for (int l = 0; l < arreqq.length; l++) {
            arreqq[l] = lista.get(l);

        }
        for (int l = 0; l < arre.length; l++) {
            arre[l] = lista1.get(l);

        }
        // ORDENO
        int aux;
        int cont = 1;
        for (int j = (arreqq.length - 1); 0 < j; j--) {

            for (int o = 0; o < j; o++) {
                if (arreqq[o] > arreqq[cont]) {

                    aux = arreqq[o];
                    arreqq[o] = arreqq[cont];
                    arreqq[cont] = aux;

                }
                cont++;
            }
            cont = 1;

        }
        String aux2 = " ";
        cont = 1;
        for (int j = (arre.length - 1); 0 < j; j--) {

            for (int o = 0; o < j; o++) {
                if (arre[o].charAt(0) > arre[cont].charAt(0)) {

                    aux2 = arre[o];
                    arre[o] = arre[cont];
                    arre[cont] = aux2;
                }
                cont++;
            }
            cont = 1;

        }

        //Leeo los dos  vectores
        System.out.println(" --------------------");
        System.out.println(" Numeros Ordenados : ");
        System.out.println(" --------------------");
        System.out.println("");
        for (int j = 0; j < arreqq.length; j++) {

            System.out.println("  " + arreqq[j]);

        }
        System.out.println("");
        System.out.println(" -----------------");
        System.out.println(" Letras Ordenadas: ");
        System.out.println(" -----------------");
        System.out.println("");
        for (int j = 0; j < arre.length; j++) {

            System.out.println("  " + arre[j]);

        }
        System.out.println(" ");
        System.out.println(" -----------------");
        System.out.println(" Vector Resultante: ");
        System.out.println(" -----------------");

        String[] arregloresul = new String[tamaño];
        String cadena = "";
        List<String> result = new ArrayList<String>();
        for (int j = 0; j < arreqq.length; j++) {

            result.add(arreqq[j] + "");
            cadena = cadena + arreqq[j] + "";
        }
        for (int j = 0; j < arre.length; j++) {

            result.add(arre[j]);
            cadena = cadena + arre[j] + "";
        }
        //presento 
        System.out.println(" ");
        System.out.println(" " + result);
        System.out.println(" ");

        //presento  el numero mas repetido
        char cad[];
        char letra;
        cad = (cadena.toLowerCase()).toCharArray();
        int cont2 = 0;

        int repet = 0;
        for (int p = 0; p < (cad.length); p++) {

            letra = cad[p];
            for (int j = 0; j < (cad.length); j++) {
                if (cad[j] == letra) {

                    cont2++;
                }
            }
            if (cont2 > repet) {

                repet = cont2;

            }
            cont2 = 0;

        }
        System.out.println(" ");
        System.out.println("------------------------------------");
        System.out.println(" El elemento que mas se repite es : ");
        System.out.println("------------------------------------");
        System.out.println(" La  maxima cantidad de  veces que se repite un determinado caracter es : " + repet+" veces ");

        List<Character> u = new ArrayList<Character>();
        cont2 = 0;
        for (int p = 0; p < (cad.length); p++) {

            letra = cad[p];
            for (int j = 0; j < (cad.length); j++) {
                if (cad[j] == letra) {

                    cont2++;
                }
            }
            if (cont2 == repet && !u.contains(cad[p])) {
                System.out.println(cad[p] + " : " + repet);
            }
            cont2 = 0;

            u.add(cad[p]);
        }

        System.out.println("");

        //Quien mas se repite 
        TFin = System.nanoTime();
        System.out.println("----------------------------------------------");
        System.out.println(" ");
        tiempo = TFin - TInicio;
        System.out.println("Tiempo de ejecución en nanosegundos: " + tiempo);
        tiempo1 = tiempo * (1.0 * 10e-9);
        System.out.println("Tiempo de ejecución en segundos: " + (tiempo1));

    }
}
