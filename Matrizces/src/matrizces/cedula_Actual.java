/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrizces;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class cedula_Actual {

    
    // METODO PARA LEER ARCHIVOS 
    // linea por linea 
    
    public List<String> leerTxt(String direccion) throws IOException {
        List<String> lista = new ArrayList<String>();
        String texto = "";

        try {
            BufferedReader bf = new BufferedReader(new FileReader(direccion));
            String temp = "  ";
            String bfRead;

            while ((bfRead = bf.readLine()) != null) {

                temp = temp + bfRead + " ";
                lista.add(bfRead);

            }

            texto = temp;

        } catch (IOException ex) {
            System.out.println(" No se encontro el archivo ");
        }
        System.out.println(" Los Datos leidos son :   " + texto);

        return lista;
    }

    public static void main(String[] args) {
        String ai = "";
        String ah = "";
        // ENTRADA PRIMER ARCHIVO 
        cedula_Actual oo = new cedula_Actual();

        try {
            // ingresamos la direccion que contiene el documento txt
            List<String> lis = oo.leerTxt("C:\\Users\\Usuario iTC\\Documents\\NetBeansProjects\\Matrizces\\Datos.txt");
            
            // Obtengo los elementos de la lista
            ai = lis.get(0);
            ah = lis.get(1);
            //........
        } catch (IOException ex) {
            System.out.println(" NO existe el documento ");
        }

        try {

            //SEGUNDO ARCHIVO 
            File mi = new File("Resultado.txt");
            FileWriter p = new FileWriter("Resultado.txt");

            Scanner lector = new Scanner(System.in);

            //
            int cont = 0;
            int[] coefValCedula = {2, 1, 2, 1, 2, 1, 2, 1, 2};

            if (ah.length() == 10) {

                String verificador1 = ah.charAt(9) + "";
                int verificador = Integer.parseInt(verificador1);
                int suma = 0;
                int digito = 0;
                for (int i = 0; i < (ah.length() - 1); i++) {
                    String p2 = ah.charAt(i) + "";
                    int o2 = Integer.parseInt(p2);
                    digito = o2 * coefValCedula[i];
                    suma = suma + ((digito % 10) + (digito / 10));
                }

                if ((suma % 10 == 0) && (suma % 10 == verificador)) {

                } else if ((10 - (suma % 10)) == verificador) {

                } else {
                    cont++;
                }
                p.write("SALIDA " + "\r\n");
                p.write("\r\n");
                p.write("\r\n");
                if (cont == 0) {
                    System.out.println("");
                    System.out.println(" La cedula es correcta ");
                    p.write("Resultado  :  La cedula es correcta " + " \r\n");
                    p.write("\r\n");
                } else {
                    System.out.println("");
                    System.out.println(" La cedula es incorrecta");
                    p.write("Resultado  :  La cedula es  incorrecta " + " \r\n");
                    p.write("\r\n");
                }
            } else {
                System.out.println("");
                System.out.println(" La cedula ingresada es incorrecta");

                p.write("Resultado  :  La cedula incorrecta  por el numero de digitos que ingreso " + " \r\n");
                p.write("\r\n");
            }

            char cadena[];

            char letra;
            int cont2 = 0;

            cadena = (ai.toLowerCase()).toCharArray();
            p.write("El numero de veces que se repite cada letra en el nombre  (  " + ai + "  ) " + " es " + "  : " + " \r\n");
            System.out.println("");
            System.out.println(" El numero de veces que se repite cada letra en el nombre  : (  " + ai + "  ) " + " es " + "  :");
            System.out.println("");
            List<Character> lista = new ArrayList<Character>();

            // el -1 para eliminar el espacio que esta leyendo del archivo
            for (int i = 0; i < (cadena.length); i++) {

                letra = cadena[i];
                for (int j = 0; j < (cadena.length); j++) {
                    if (cadena[j] == letra) {

                        cont2++;
                    }
                }
                if (!lista.contains(cadena[i])) {
                    p.write(letra + " :  " + cont2 + " \r\n");

                    System.out.println(letra + " : " + cont2);
                    cont2 = 0;
                }
                cont2 = 0;
                lista.add(cadena[i]);

            }

            p.write("\r\n");

            p.write("! Gracias por su visita ! " + "\r\n");
            p.close();
        } catch (IOException ex) {
            System.out.println(" Error");
        }
    }

}
