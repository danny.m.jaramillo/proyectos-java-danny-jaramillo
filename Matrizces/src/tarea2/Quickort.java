/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class Quickort {

    public static void main(String[] args) {
        Scanner pantalla = new Scanner(System.in);
        int[] numeros = new int[8];
        int num = 0;

        for (int i = 0; i < 8; i++) {
            System.out.println("---------------------");
            System.out.println("|Ingrese un numero :|");
            System.out.println("---------------------");
            num = pantalla.nextInt();

            numeros[i] = num;
        }
        System.out.println("-----------------------------------");
        System.out.println("|El vector orinal es el siguiente :|");

        System.out.println(" ");
        for (int i = 0; i < 8; i++) {

            System.out.print(" " + numeros[i] + "  ");
        }
        System.out.println("");
        System.out.println("-----------------------------------");

        int pivo = 7 / 2;
        int aux = 0;
        int pas = 0;
        int l = 0;
        int y = 7;
        for (int j = 7; 0 < j; j--) {

            if (!(numeros[l] < numeros[pivo] && numeros[y] > numeros[pivo])) {
                aux = numeros[l];
                numeros[l] = numeros[j];
                numeros[y] = aux;
                l++;
                y--;
            }
            if (numeros[l] < numeros[pivo]) {
                l++;
            } else if (numeros[y] > numeros[pivo]) {
                l--;
            }

            if (l == (pivo - 1) && y == (pivo + 1)) {

                y = pivo;
                l = 0;
                pivo=y/2;

            }
        }

        System.out.println("-------------------------------------------------------------------------");
        System.out.println(" El vector ordenado dela forma Ascendente termino de la siguiente manera :");
        System.out.println("-------------------------------------------------------------------------");
        for (int i = 0; i < 15; i++) {

            System.out.print(" " + numeros[i] + "  ");
        }
    }
}
