/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class MetodoBurbuja {
//Ordenar un arreglo estatico en forma Ascendente
//Danny Jaramillo  Jumbo,Alexis Quishpe Mendoza,Wilson Valverde Jadan

    public static void main(String[] args) {
        Scanner pantalla = new Scanner(System.in);
        int[] numeros = new int[15];
        int num = 0;

        for (int i = 0; i < 15; i++) {
            System.out.println("---------------------");
            System.out.println("|Ingrese un numero :|");
            System.out.println("---------------------");
            num = pantalla.nextInt();

            numeros[i] = num;
        }
        System.out.println("-----------------------------------");
        System.out.println("|El vector orinal es el siguiente :|");
        
        System.out.println(" ");
        for (int i = 0; i < 15; i++) {

            System.out.print(" " + numeros[i] + "  ");
        }
        System.out.println("");
        System.out.println("-----------------------------------");
        int aux;
        int cont = 1;

        for (int j = 14; 0 < j; j--) {

            for (int i = 0; i < j; i++) {
                if (numeros[i] > numeros[cont]) {
                    aux = numeros[i];
                    numeros[i] = numeros[cont];
                    numeros[cont] = aux;
                }
                cont++;
            }
            cont = 1;

        }

        System.out.println("-------------------------------------------------------------------------");
        System.out.println(" El vector ordenado dela forma Ascendente termino de la siguiente manera :");
        System.out.println("-------------------------------------------------------------------------");
        for (int i = 0; i < 15; i++) {

            System.out.print(" " + numeros[i] + "  ");
        }
    }
}
