/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Danny Jaramillo
 */
public class Vendedor {

    /**
     * @return the productos
     */
    public List<String> getProductos() {
        return productos;
    }

    /**
     * @param productos the productos to set
     */
    public void setProductos(List<String> productos) {
        this.productos = productos;
    }

    /**
     * @return the clientes
     */
    public List<String> getClientes() {
        return clientes;
    }

    /**
     * @param clientes the clientes to set
     */
    public void setClientes(List<String> clientes) {
        this.clientes = clientes;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the contraseña
     */
    public String getContraseña() {
        return contraseña;
    }

    /**
     * @param contraseña the contraseña to set
     */
    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    private String nombre;
    private String contraseña;
    
    String Numeroventas;
    

    private List<String> productos = new ArrayList<>();
    private List<String> clientes = new ArrayList<>();
}
