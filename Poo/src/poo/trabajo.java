/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class trabajo {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Ingrese un numero binario");
        int binary = reader.nextInt();
        
        /**
         * Creo un objeto de la clase / el metodo pude crearse en la misma clase o en una clase aparte es un metodo de tipo publico que resive  un parametro y sin retorno .
         */

        
         trabajo a = new trabajo();
         a.calcularDecimal(binary);
    }

    public void calcularDecimal(int binary) {
        int decimal = 0;

        int power = 0;

        while (binary != 0) {
            int lastDigit = binary % 10;
            decimal += lastDigit * Math.pow(2, power);
            power++;
            binary = binary / 10;
        }
        System.out.println(decimal);

    }

}
