/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.unl.rrhh;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import poo.Herramientas;

/**
 *
 * @author Usuario
 */
public class GestionRh {
    
    public static void main(String[] args) {

        /// ESATTICOS 
//        pruebEstaticos();
        clseString();
//        int edadE = 12;

//        boolean esMayor = Herramientas.edadMinima < edadE;
//        poo.Herramientas.verificarNumeroIdentificacion("1234");
//        //BUCLEE
//        String y;
//        String n;
//        //lista no de objetos  List cualquiertipo=new ArrayList();
//        //lista de objetos
//        
//        List<Persona> personas = new ArrayList<Persona>();
//        Scanner lector = new Scanner(System.in);
//
//        for (int i = 1; i <= 3; i++) {
////            DEBOMOS CREAR CADA VES UNA NUEVA PERSONA PARA QEU AGREGE A PERSONAS DIFERENTES AL METODO
//
//            Persona o12 = new Persona();
//            System.out.println(" Ingrese el nombre "+i);
//            n = lector.nextLine();
//            o12.setNumbres(n);
//            personas.add(o12);
//            System.out.println(" ingrese el numero de cedula ");
//            y = lector.nextLine();
//            o12.setCedula(y);
//
//            if (poo.Herramientas.verificarNumeroIdentificacion(y)) {
//                personas.add(o12);
//            } else {
//               
//                boolean a = true;
//
//                while (a) {
//
//                    System.out.println(" LA CEDULA ESTA MAL INGRESA INGRESE NUEAVMENTE  LA CEDULA");
//                    System.out.println(" ingrese el numero de cedula ");
//                    y = lector.nextLine();
//                    o12.setCedula(y);
//                    if (poo.Herramientas.verificarNumeroIdentificacion(y)) {
//                        personas.add(o12);
//                        a=false;
//                    }
//                }
//                
//            }
//
//        }
//
//        for (int i = 0; i < personas.size(); i++) {
//            System.out.println("   ");
//            System.out.println("   ");
//
//            Persona cedula = personas.get(i);
//
//            cedula.imprimirDatos();
//
//        }
//        creamos un objeto 
//constructor es un metodo que pertenece a una clase new Persona();
//        Persona person1 = new Persona();
//        //DENTRO DEL PERSONA(cntrl espacio) y escojo el constructor que quiero
//        Persona person2 = new Persona("10023343", "nombres Pedro");
//        Persona person3 = new Persona("122323", "asfsdfds", "sdfdgg", "asdgfd");
////        objeto string
//        String ob = new String("valor");
//        person1.setNumbres("Juan Carlos");
//        person1.setTelefono("1223455");
//        person1.setDirecion("BARRIO ARGELIA");
//        //llamo metodos
//        person1.imprimirDatos();
//        person2.imprimirDatos();
//        person3.imprimirDatos();
//
//        //presento
//        System.out.println(person1.getNumbres());
//        System.out.println();
//
//        if (person2.getNumbres() != null) {
//            System.out.println(person2.getNumbres());
//
//        }
//        //CLASE EMPLEADO
//        Empleado emp = new Empleado();
//        emp.setCodigoUnl(1);
//        emp.setNumbres("pedro");
//        emp.setDirecion("direccion de Pedro");
//        System.out.println("");
//        System.out.println(emp.getNumbres());
//        System.out.println(emp.getDirecion());
//        System.out.println(emp.getCodigoUnl());
//        String nomm = emp.getNumbres();
    }
    
    public static void pruebEstaticos() {
        //DEVUELBE EL MAYOR DE DOS NUMEROS DOUBLES

        System.out.println(Math.max(3.4, 7));

        //DEVUELBE UN solo la parte entera redonde al numero siguiente devuelbe doble
        System.out.println(Math.ceil(3.2));

        //DEVUELBE UN solo la parte entera
        System.out.println(Math.floor(3.82342));
        //REDONDE AL ENTERO MaS CERCANO solo devuebe un entero
        System.out.println(Math.round(3.82342));

        //POTENCIA 4 ELEVADO AL 3 
        System.out.println(Math.pow(4, 3));
        //numeros aleatorios

        System.out.println(Math.random() * 50);

        //metodos de fechas
        Date fecha = new Date();
        
        System.out.println(fecha.toString());
        
        Date fecha2 = new Date(2000000000000L);
        System.out.println(fecha.toString());
        
        Calendar call = Calendar.getInstance();
//          System.out.println(call.get(Calendar.MONTH));
//          System.out.println(call.get(Calendar.DAY_OF_WEEK));
//          
//          PRESENTAR FECHA
//          System.out.println(call.get(Calendar.YEAR)+" / "+call.get(Calendar.MONTH)+" /  "+call.get(Calendar.DAY_OF_MONTH));
//          
//          int actual;
//          int result;
//          System.out.println(" La hora actual");
//          actual=call.get(Calendar.HOUR);
//          System.out.println(actual);
//          System.out.println("LA hora restada es (-180 minutos)");
//          result=actual-3;
//          System.out.println(result);
//          
//          System.out.println("---------------------");
//          call.add(Calendar.HOUR,8);
//          System.out.println(call);
        String fechaa = " ";
        fechaa += call.get(Calendar.YEAR) + " /";
        fechaa += call.get(Calendar.MONTH) + 1 + " /";
        fechaa += call.get(Calendar.DAY_OF_MONTH) + " /";
        fechaa += call.get(Calendar.HOUR) + " /";
        fechaa += call.get(Calendar.MINUTE) + " /";
        fechaa += call.get(Calendar.SECOND);
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" actual .............");
        System.out.println(fechaa);

        ///modifico horas
        call.add(Calendar.HOUR, -180);
        
        fechaa = call.get(Calendar.YEAR) + " /";
        fechaa += call.get(Calendar.MONTH) + 1 + " /";
        fechaa += call.get(Calendar.DAY_OF_MONTH) + " /";
        fechaa += call.get(Calendar.HOUR) + " /";
        fechaa += call.get(Calendar.MINUTE) + " /";
        fechaa += call.get(Calendar.SECOND);
        
        System.out.println("Restado...............");
        System.out.println(" " + fechaa);
    }

    //METODO ESTRING
    public static void clseString() {
        String s1 = "string1";
        String cad = " ";
        String s2 = new String("string2");
        
        System.out.println("----------------");
        for (int i = s1.length() - 1; i >= 0; i--) {
            //concateno
            cad += s1.charAt(i);
        }
        System.out.println(cad);
        System.out.println("mas metodos----------------");
        //devuelbe la pocicond eun caracter
        System.out.println(cad.indexOf("n"));
        //remplasa una cadena por otra
        System.out.println(cad.replace("ir", "abc"));
        //combierte un dato en estring
        System.out.println(cad.valueOf(123));
        //combierte a mayusculas lo que tiene la cadena
        System.out.println(cad.toUpperCase());
        //combierte a minusculas la cadena
        //VARIABLE.TOLOWRCASE
        System.out.println("JKKKKLJ".toLowerCase());
        //DIVIDIDE UNA CADENA segunn el caracter que pongamos
        //seapra cada elemento y lo posiciona en el vector al encontrar el caracter previamente ingressado entre los parentecis ojo entre los parentecis
        String[] arr="1,2,45,6".split(",");
        for(String r:arr){
            System.out.println(r);
        }
    }
    
}
