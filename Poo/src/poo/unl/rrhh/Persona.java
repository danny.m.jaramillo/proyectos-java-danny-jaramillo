/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.unl.rrhh;

/**
 *
 * @author Usuario
 */
public class Persona {
//    clase instancia objetos
    //cambiamos todas a private por eso marca error en la calse que la ejecuta

    private String cedula;
    private String numbres;//solo ingresan desde el mismo paquete
    private String telefono;//solo en el mismo paquete
    private String direcion;//desde cualquier paquete
//private gety set  clic derecho,  refactor,encampsulamiento, selecionamos all , refactor

    public String getCedula() {
        return this.cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the numbres
     */
    public String getNumbres() {
        return numbres;
    }

    /**
     * @param numbres the numbres to set
     */
    public void setNumbres(String numbres) {
        this.numbres = numbres;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the direcion
     */
    public String getDirecion() {
        return direcion;
    }

    /**
     * @param direcion the direcion to set
     */
    public void setDirecion(String direcion) {
        this.direcion = direcion;
    }
    // si tengo un constructor debo declarar el constructor base sino marca error

    public Persona() {

    }

    //constructor
    public Persona(String cedula, String nombres) {
        this.numbres = nombres;
        this.cedula = cedula;
    }
//constructor numero dos nombre de  clase y doble clic genera un constructor con todas las variables y fijando los valores

    public Persona(String cedula, String numbres, String telefono, String direcion) {
        this.cedula = cedula;
        this.numbres = numbres;
        this.telefono = telefono;
        this.direcion = direcion;
    }

    //METODOS////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void imprimirDatos() {
        System.out.print("Nombre " + this.numbres + " Cedula" + this.cedula + " Direccion" + this.direcion + " Telefono" + this.telefono);

//        if (this.numbres != null) {
//
//        }
    }
}
