/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.unl.rrhh;

/**
 *
 * @author Usuario
 */
//heredo on EXTENS nombre de la clase que ereda(Persona)
public class Empleado extends Persona {

    private int codigoUnl;

    /**
     * @return the codigoUnl
     */
    public int getCodigoUnl() {
        return codigoUnl;
    }

    /**
     * @param codigoUnl the codigoUnl to set
     */
    public void setCodigoUnl(int codigoUnl) {
        this.codigoUnl = codigoUnl;
    }
}
