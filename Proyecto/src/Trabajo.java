/*
 * To change this license header, choose License Heade1*rs in Project PropertiesX.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
//D.J.A.F. A.M
 */


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class Trabajo {

    //PRIMERA PREGUNTA
    // Primer Metodo
    public int[] crearMatriz(int tamaño, int limiteSuperior, int limiteinferior) {
        int[] a = new int[tamaño];
        for (int i = 0; i < tamaño; i++) {
            a[i] = (int) Math.floor(Math.random() * (limiteSuperior - limiteinferior)) + limiteinferior;

        }

        return a;

    }
    //Segundo Metodo

    public int[][] crearMatriz(int filas, int columnas, int limiteSuperior, int limiteinferior) {
        int[][] a = new int[filas][columnas];

        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                a[i][j] = (int) Math.floor(Math.random() * (limiteSuperior - limiteinferior)) + limiteinferior;

            }

        }

        return a;

    }

    //Tercer Metodo
    public List<Integer> crearLista(int tamaño, int limiteSuperior, int desde) {
        List<Integer> lista = new ArrayList<Integer>(tamaño);

        for (int i = 0; i < tamaño; i++) {
            int a = (int) Math.floor(Math.random() * (limiteSuperior - desde)) + desde;
            lista.add(a);
        }
        return lista;

    }
//SEGUNDA PREGUNTA

    public int[] Array(int tamaño) {
        int[] a = new int[tamaño];
        for (int i = 0; i < tamaño; i++) {
            a[i] = (int) Math.floor(Math.random() * (30 - 10)) + 10;

        }
        for (int i = 0; i < tamaño; i++) {
            System.out.println(" La posicion es :  " + i + " y contiene el valor  :  " + a[i]);

        }
        return a;

    }

    //TERCERA PREGUNTA
    public int fibonasi(int cantidad_Numeros) {

        int[] a = new int[cantidad_Numeros];
        int vi = 1;
        int vf = 1;
        int aux = 0;
        a[0] = vi;
        System.out.println(" " + a[0]);
        for (int i = 1; i < cantidad_Numeros; i++) {
            a[i] = vf;

            aux = vf;
            vf = vi + vf;

            vi = aux;
            System.out.println(" " + a[i]);
        }

        return 0;

    }
    //CUARTA PREGUNTA

    public List<Integer> crearLista2(int tamaño, int limiteSuperior, int desde) {
        List<Integer> lista = new ArrayList<Integer>(tamaño);

        for (int i = 0; i < tamaño; i++) {
            int a = (int) Math.floor(Math.random() * (limiteSuperior - desde)) + desde;
            lista.add(a);
        }

        //desordenado 
        System.out.println("----------------------------------");
        System.out.println(" Lista desordenada                ");
        System.out.println("----------------------------------");
        for (int i = 0; i < lista.size(); i++) {

            System.out.println(lista.get(i));
        }
        // metodo para ordenarunalista dle menor al mayor
        Collections.sort(lista);
        System.out.println("----------------------------------");
        System.out.println(" Lista Ordenada del menor al mayor");
        System.out.println("----------------------------------");
        for (int i = 0; i < lista.size(); i++) {

            System.out.println(lista.get(i));
        }
        return lista;

    }

    // PREGUNTA 5
    public List<Integer> crearLista3(int tamaño) {
        List<Integer> lista = new ArrayList<Integer>(tamaño);
        double media = 0;
        System.out.println("El tamaño de la lista sera : " + tamaño);
        for (int i = 0; i < tamaño; i++) {
            int a = (int) Math.floor(Math.random() * (1000 - 1)) + 1;
            media = a + media;
            lista.add(a);
        }
        System.out.println(" La media es  : " + (media / tamaño));
        int b = lista.get(0);
        for (int i = 0; i < tamaño; i++) {
            System.out.println(" " + lista.get(i));

            if (lista.get(i) > b) {

                b = lista.get(i);
            }

        }
        System.out.println("El elemento mayor de la lista es :  " + b);

        int h = lista.get(0);
        for (int i = 0; i < tamaño; i++) {

            if (lista.get(i) < h) {

                h = lista.get(i);
            }

        }
        System.out.println("El elemento menor de la lista es :  " + h);
        return lista;

    }

//    PREGUNTA 6
    public int[][] crearMatriz2(int tamañodelasMatrices) {
        int[][] a = new int[tamañodelasMatrices][tamañodelasMatrices];
        int[][] b = new int[tamañodelasMatrices][tamañodelasMatrices];
        List<Integer> l1 = new ArrayList<Integer>();
        List<Integer> l2 = new ArrayList<Integer>();

        int cont = 0;
        for (int i = 0; i < tamañodelasMatrices; i++) {
            for (int j = 0; j < tamañodelasMatrices; j++) {
                int y;
                a[i][j] = (int) Math.floor(Math.random() * (10 - 0)) + 0;
                y = a[i][j];
                l1.add(y);
                //en la misma linea
                System.out.print(" " + a[i][j] + "  ");

                if (cont == (tamañodelasMatrices - 1)) {
                    //salto de linea

                    System.out.println("  ");
                }
                cont++;
            }
            cont = 0;

        }
        System.out.println(" La segunda matriz es :");

        System.out.println(" ");
        //segunada matriz
        for (int i = 0; i < tamañodelasMatrices; i++) {
            for (int j = 0; j < tamañodelasMatrices; j++) {
                int y;
                b[i][j] = (int) Math.floor(Math.random() * (10 - 0)) + 0;
                y = b[i][j];
                l2.add(y);
                //en la misma linea
                System.out.print(" " + b[i][j] + "  ");

                if (cont == (tamañodelasMatrices - 1)) {
                    //salto de linea

                    System.out.println("  ");
                }
                cont++;
            }
            cont = 0;

        }

        List<Integer> l3 = new ArrayList<Integer>();
        System.out.println(" ");
        System.out.println(" Los elementos que se repiten son  :  ");
        System.out.println("");
        for (int i = 0; i < l1.size(); i++) {

            for (int j = 0; j < l2.size(); j++) {

                if (l1.get(i) == l2.get(j)) {

                    if (!l3.contains(l1.get(i))) {
                        l3.add(l1.get(i));
                    }

                }

            }

        }

        for (int i = 0; i < l3.size(); i++) {

            System.out.println("* " + l3.get(i));

        }
        return a;
    }

    //TRABAJO 7
    public int Letras() {
        List<Character> l3 = new ArrayList<Character>();

        int j = 0;
        int b;
        System.out.println("Creo la longitud del for asta el 26 debedio ah que son 26 letras mayusculas representadas en el codigo ASCCI ( sin contar la Ñ) como se generar aleatoriamnete si se repiten  no las agrego y si no se repiten las agrego a la Lista");
        for (int i = 0; i < 26; i++) {
            b = (int) Math.floor(Math.random() * (90 - 65)) + 65;
            char h = (char) b;

            if (!l3.contains(h)) {
                l3.add(h);

            }

        }

        System.out.println("Las letras generadas en  orden   Aleatorio  entre la A y la Z son : ");
        for (int i = 0; i < l3.size(); i++) {

            System.out.println(l3.get(i));

        }

        return 0;

    }

    //PREGUNTA 8
    public int mes(int NumerodeMes) {
        System.out.println(" El numero que Ingreso es : " + NumerodeMes);
        System.out.println(" El mes al que equivale es :");
        if (NumerodeMes == 1) {

            System.out.println("  Enero ");
        } else if (NumerodeMes == 2) {
            System.out.println("  Febrero ");
        } else if (NumerodeMes == 3) {
            System.out.println("  Marzo ");
        } else if (NumerodeMes == 4) {
            System.out.println("  Abril ");
        } else if (NumerodeMes == 5) {
            System.out.println("  Mayo ");
        } else if (NumerodeMes == 6) {
            System.out.println("  Junio ");
        } else if (NumerodeMes == 7) {
            System.out.println("  Julio ");
        } else if (NumerodeMes == 8) {
            System.out.println(" Agosto ");
        } else if (NumerodeMes == 9) {
            System.out.println("  Septiembre ");
        } else if (NumerodeMes == 10) {
            System.out.println("  Octubre ");
        } else if (NumerodeMes == 11) {
            System.out.println("  Noviembre ");
        } else if (NumerodeMes == 12) {
            System.out.println("  Diciembre ");
        } else {
            System.out.println(" El numero ingresado no corresponde a ningun Mes del Año");
        }

        return 0;

    }
    //PREGUNTA 9

    public int ingles(String MesEspañol) {
// la contraseña debe ser exacatamente la misma , es decir ingresar en minuscula uno de los 12 meces en español
        HashMap diccionario = new HashMap();

        diccionario.put("enero", " January ");
        diccionario.put("febrero", " February ");
        diccionario.put("marzo", " March ");
        diccionario.put("abril", " April ");
        diccionario.put("mayo", " May ");
        diccionario.put("junio", " June ");
        diccionario.put("julio", " July ");
        diccionario.put("agosto", " August ");
        diccionario.put("septiembre", " September ");
        diccionario.put("octubre", " October ");
        diccionario.put("diciembre", " December");

        String result = diccionario.get(MesEspañol).toString();
        System.out.println(" La tradcuuion al ingles es   : " + result);
        return 0;

    }

    Scanner lector = new Scanner(System.in);
    //PREGUNTA 10

    public int Conversor() {
        double r;
        double ingres;

        int a = 0;
        System.out.println(" Puede Ingresar unicamente dos valores enteros (1) y (2)");
        System.out.println(" (1) centigrados>farenheit");
        System.out.println(" (2) farenheit>centigrados");
        System.out.println(" Ingrese su Opcion");
        int y = lector.nextInt();

        if (y == 1) {
            System.out.println(" Ingrese los Grados Centigrados");
            ingres = lector.nextDouble();
            r = (9.0 / 5.0 * (ingres));
            System.out.println(" El valor resultante es : " + r + "  Farenheit");
        } else if (y == 2) {
            System.out.println(" Ingrese los Grados Farenheit");
            ingres = lector.nextDouble();
            r = (5.0 / 9.0 * (ingres));
            System.out.println(" El valor resultante es : " + r + "  Centigrados");
        } else {
            System.out.println(" Ingreso un valor que no correspondia aninguna opcion.");
        }
        return 0;

    }

    //PREGUNTA 11
    public int bonos() {
        int años;
        int total = 0;
        int mayor = 0;
        List<Integer> l3 = new ArrayList<Integer>();

        System.out.println(" Cual es minimo de años para que un Empleado  acceda al Bono:");
        años = lector.nextInt();
        System.out.println(" Ingrese el numero de Empleados que va a Ingresar");
        int empl = lector.nextInt();
        for (int i = 1; i <= empl; i++) {
            System.out.println(" Ingrese la cantidad de años trabajados del Enpleado Numero   :" + i);
            int bono = lector.nextInt();

            l3.add(bono);
        }

        System.out.println("--------------------------------------------------------------------");

        for (int i = 0; i < l3.size(); i++) {

            if (l3.get(i) >= 20) {

                mayor++;

            }
            if (l3.get(i) >= años) {
                total++;
            }
        }

        System.out.println(" El total de bonos que la Empresa entrega son :  " + total);
        System.out.println(" ");
        System.out.println(" El total de empleados mayores de 20 años  son :  " + mayor);

        System.out.println(" El procentaje  con respecto al total de bonos entregados (100 %) que reciben de  cada Empleado , con un total de años de trabajo mayor o igual ah  " + años + "años.");
        System.out.println(" El porcentaje que resibe cada empleado es de  " + (100 / total) + " % ");
        return 0;
    }

//    PREGUNTA 12
    public int Horas(int segundos) {

        int horas;
        int min;
        int seg;

        horas = segundos / 3600;
        min = (segundos - (horas * 3600)) / 60;
        seg = segundos - ((horas * 3600) + (min * 60));

        System.out.println("El resultado tranformado qeudaria");
        System.out.println(" " + horas + "  H  : " + min + " min : " + " : " + seg + " seg");
        return 0;

    }

    //    PREGUNTA 13
    public int Tabla() {
        System.out.println(" Ingrese un numero para generar la tabla de Multiplicar correspondiente ");
        int num = lector.nextInt();
        int mult;
        System.out.println("--------------");
        for (int i = 1; i <= 10; i++) {

            mult = num * i;
            System.out.println("" + num + " * " + i + " = " + mult);

        }
        return 0;
    }

    //    PREGUNTA 14
    public int Filas() {
        System.out.println(" Ingrese la cantidad de N  numeros naturales que decea generar ");
        int num = lector.nextInt();
        System.out.println(" ---------------------------------------------------------------");
        System.out.println("");

        int r = 1;

        int[] a = new int[num];
        a[0] = 1;
        for (int i = 1; i < num; i++) {

            a[i] = (r + 2);
            r = (r + 2);

        }
        int y = 1;
        int cont = 0;
        for (int i = 0; i < num; i++) {

            for (int j = 0; j < num; j++) {

                if (j < y) {
                    //en la misma linea
                    System.out.print(" " + a[j] + "  ");

                } else if (cont == 0) {
                    System.out.println(" ");
                    cont = 2;
                }

            }
            y++;
            cont = 0;
        }
        return 0;

    }

    //    PREGUNTA 15
    public int Num() {

        //UTILIZAMOS EL TIPO DE DATO LONG DEBDIO A QUE LAS CANTIDADES AXCEDEN LA LONGITUD DEL TIPO int
        System.out.println(" Ingrese la cantidad de  veces que decea generar la Serie");
        int num = lector.nextInt();
        System.out.println(" ---------------------------------------------------------------------");
        long suma = 0;
        long a = 2;
        long u;
        long m;

        long b;
        System.out.println(" El numero inicial del que se genera la serie es : " + a);
        System.out.print(" [ 2 ] ");
        suma = suma + a;
        for (int i = 1; i <= num; i++) {
            System.out.print("  | ");
            u = (int) Math.pow(a, 2);
//            System.out.println(" " + a);
            b = u * 2;
//            System.out.println(" " + b);
            m = b - 2;
            suma = suma + u + b + m;

            System.out.print("  " + u + " ,  " + b + " , " + m);
            a = m;
            System.out.print("  |  ");
        }
        System.out.println(" ");
        System.out.println("------------------------------------");
        System.out.println(" El resultado de la Suma es : " + suma);

        return 0;
    }

    //Pregunta 16 
    public int serie() {

        System.out.println(" Ingrese la cantidad del tamaño de la serie : ");
        int num = lector.nextInt();
        int n = 1;
        int y = 1;
        int cont = 1;
        System.out.println("-----------------------------------------------");
        for (int i = 1; i <= num; i++) {

            System.out.print("  " + n);
            n = - 1 * n;

            if (!(cont < 2)) {

                n = (y + 1);
                y = n;
                cont = 0;
            }

            cont++;
        }
        System.out.println(" ");

        return 0;
    }

    //PREGUNTA 17
    public int sorteo() {

        int b = (int) Math.floor(Math.random() * (50 - 1)) + 1;
        int lims = 50;
        int limin = b - (b / 2);
        int cont = 0;
        for (int i = 1; i <= 5; i++) {
            System.out.println("--------------------------------------------------");
            System.out.println(" Ingrese el numero del Intento Numero  " + i + " ");
            System.out.println("--------------------------------------------------");
            int num = lector.nextInt();

            if (cont > 1 && b != 50) {

                lims = b + 1;

            }

            if (num == b) {
                System.out.println(" ! Felicidades , adivinaste el numero ! ");
                i = 6;
            } else {
                System.out.println(" El numero que estoy pensando esta entre " + limin + "  y " + lims);
            }

            cont++;

            if (i == 5 && num != b) {
                System.out.println("--------------------------------------------------");
                System.out.println("Se acabaron los intentos el nuemro que pense era : " + b);
            }
        }

        return 0;
    }

//PREGUNTA  18
    public int num() {
        System.out.println(" Ingrese la longitud ");
        int num = lector.nextInt();
        int res = num;
        int cos = num;

        List<Integer> l3 = new ArrayList<Integer>(num);
        for (int i = 0; i < num; i++) {

            for (int j = 0; j < res; j++) {
                l3.add(cos);

            }
            cos = cos - 1;
            res = res - 1;

        }
        System.out.println("--------------------------------");
        int y = num - 1;
        int u = 1;
        for (int i = 0; i < l3.size(); i++) {

            System.out.print("   " + l3.get(i));
            if (i == y) {
                System.out.println(" ");

                y = y + (num - 1);
                num = num - u;
            }
        }
        System.out.println("   ");
        return 0;
    }

    //PREGUNTA 19
    public int cuboperfect() {

        System.out.println(" Ingrese el numero ");
        String num = lector.next();
        System.out.println("----------------------------------");
        int num1 = Integer.parseInt(num);

        int l = num.length();
        int suma = 0;
        for (int i = 0; i < l; i++) {
            int k = ((int) num.charAt(i)) - 48;

            suma = suma + (int) Math.pow(k, 3);

        }
        System.out.println(" EL numero ingresado fue :  " + num);
        System.out.println("---------------------------------");

        System.out.println(" Elevando cada numero individualmente al cubo el resultado es : " + suma);
        System.out.println("-----------------------------------");
        System.out.println("Es un cubo perfecto  ( 1 ) ");
        System.out.println("No es un cubo  perfecto  ( 0 ) ");
        System.out.println("-----------------------------------");
        if (num1 == suma) {
            System.out.println("El resultado es :  1 ");
        } else {
            System.out.println("El resultado es :  0 ");
        }

        return 0;

        //En caso de ser de  entero a caracter es + 48 
        // int c = '1';   char n =1;
        //Para pasar de carácter a número: int n = (int) (c - 48);
        //Para pasar de número a carácter: char c = (char) (n + 48);
    }

    //PREGUNTA 20
    public int Transpuesta() {
        System.out.println(" Trabajamos con datos ya establecidos de una Matriz");
        System.out.println("----------------------------------------------------");
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {10, 11, 12}};
        int[][] matrizT = new int[matriz[0].length][matriz.length];
        int x, y;

        for (x = 0; x < 4; x++) {
            for (y = 0; y < 3; y++) {
                System.out.print(matriz[x][y] + "\t");
            }
            System.out.println("");
        }
        System.out.println("");

        for (x = 0; x < matriz.length; x++) {
            for (y = 0; y < matriz[x].length; y++) {
                matrizT[y][x] = matriz[x][y];
            }
        }
        System.out.println("Resultado en Matriz Transpuesta");
        System.out.println("----------------------------------------------------");

        for (x = 0; x < matriz[x].length; x++) {
            for (y = 0; y < matriz.length; y++) {
                System.out.print(matrizT[x][y] + "\t");
            }
            System.out.println();
        }

        return 0;
    }

    public static void main(String[] args) {
        Trabajo obj = new Trabajo();
        Scanner pantalla = new Scanner(System.in);
        // MENU

        boolean opcion = true;

        do {
            System.out.println(" --------------------------------------------------------------");
            System.out.println(" ------------------  LISTA DE OPCIONES   ----------------------");
            System.out.println(" --------------------------------------------------------------");
            System.out.println("| [2] Crear un Array de Numeros.                              |");
            System.out.println("| [3] Matriz Fibonacci.                                       |");
            System.out.println("| [4] Lista de Numeros aleatorios.                            |");
            System.out.println("| [5] Lista de Enteros aleatorios( pedir por teclado).        |");
            System.out.println("| [6] Crear Arrays Bidimencionales del mismo tamaño.          |");
            System.out.println("| [7] Crear Arrays con letras Mayusculas.                     |");
            System.out.println("| [8] Pedir un numero entre(1-12)devolver  elnombre del mes.  |");
            System.out.println("| [9] Diccionario de Español al Ingles.                       |");
            System.out.println("| [10] Transformar a Grados Centigrados  o a Grados Farenheit.|");
            System.out.println("| [11] Total de Bonos  que paga la empreza.                   |");
            System.out.println("| [12] Transforams segundos a  [ hh:mm:ss ].                  |");
            System.out.println("| [13] Tabla de Multiplicar.                                  |");
            System.out.println("| [14] N filas de numeros Naturales.                          |");
            System.out.println("| [15] Sucesion de numeros  y calcular la suma de los mismos. |");
            System.out.println("| [16] Obtener la serie 1,-1,2,-2,3,-3 para n numeros.        |");
            System.out.println("| [17] Adivinar un Numeros entre (1-50).                      |");
            System.out.println("| [18] Generar secuecia asta una longitud N.                  |");
            System.out.println("| [19] Cubos perfecto.                                        |");
            System.out.println("| [20] Imprimir una Matris Transpuesta.                       |");
            System.out.println("| ------------------------------------------------------------|");
            System.out.println("| ------------------------------------------------------------|");
            System.out.println("");
            System.out.println("----------------------------");
            System.out.println("|     Escoja una Opcion     |");
            System.out.println("----------------------------");
            int op = pantalla.nextInt();
            int o;
            int i;
            int e;
            switch (op) {

                case 2:
                    System.out.println(" Ingrese el tamaño que desea :");
                    o = pantalla.nextInt();
                    System.out.println("-----------------------------");
                    obj.Array(o);

                    break;
                case 3:
                    System.out.println(" Ingrese el total de numeros que quiere obtener:");
                    o = pantalla.nextInt();
                    System.out.println("------------------------------------------------");
                    obj.fibonasi(o);

                    break;

                case 4:
                    System.out.println(" Ingrese el tamaño de la Lista:");
                    o = pantalla.nextInt();
                    System.out.println(" ------------------------------");
                    System.out.println(" Ingrese el limite superior:");
                    i = pantalla.nextInt();
                    System.out.println(" ------------------------------");
                    System.out.println(" Ingrese el limite inferior :");
                    e = pantalla.nextInt();
                    System.out.println(" ------------------------------");

                    obj.crearLista2(o, i, e);

                    break;

                case 5:
                    System.out.println(" Ingrese el tamaño ");
                    o = pantalla.nextInt();
                    System.out.println("------------------------------------------------");
                    obj.crearLista3(o);

                    break;

                case 6:
                    System.out.println(" Ingrese el tamaño de la matriz ");
                    o = pantalla.nextInt();
                    System.out.println("------------------------------------------------");
                    obj.crearMatriz2(o);

                    break;

                case 7:

                    obj.Letras();

                    break;

                case 8:
                    System.out.println(" Ingrese un numero entre  el 1 y el 12 ");
                    o = pantalla.nextInt();
                    System.out.println("------------------------------------------------");

                    obj.mes(o);

                    break;

                case 9:
                    System.out.println(" Ingrese el nombre del mes en español ( unicamente en minusculas) ");
                    String r = pantalla.next();

                    obj.ingles(r);

                    break;
                case 10:

                    obj.Conversor();

                    break;
                case 11:

                    obj.bonos();

                    break;

                case 12:
                    System.out.println(" Ingrese los segundos");
                    o = pantalla.nextInt();

                    obj.Horas(o);

                    break;

                case 13:

                    obj.Tabla();

                    break;

                case 14:

                    obj.Filas();

                    break;

                case 15:

                    obj.Num();

                    break;

                case 16:

                    obj.serie();

                    break;
                case 17:

                    obj.sorteo();

                    break;
                    
                    
                     default:
                    System.out.println(" El numero ingrasado es incorrecto escoja entre el intervalo ESTABLECIDO. ");
                    break;

                case 18:

                    obj.num();

                    break;

                case 19:

                    obj.cuboperfect();

                    break;
                case 20:

                    obj.Transpuesta();

                    break;

               
            }
            System.out.println(" ");
            System.out.println("-----------------------------------------");
            System.out.println("[    Decea realizar otra Opercaion      ]");
            System.out.println("-----------------------------------------");
            System.out.println("  ");
            String q = pantalla.next();

            if (q.equalsIgnoreCase("no")) {

                System.out.println("---------------------------");
                System.out.println(" ! Gracias por su visita ! ");
                System.out.println("---------------------------");

                opcion = false;
            }

        } while (opcion);
        System.out.println("");
        System.out.println("");
        System.out.println(" Ejecucion finalizada.");
        System.out.println("---------------------------------------------------------------------------------");
        System.out.println("-Desarrolladores: Sr. Danny Jaramillo ,Sr.Alexander Faican ,Sr. Alexis Maldonado-");
        System.out.println("---------------------------------------------------------------------------------");
    }

}
