/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leerescribira;

import java.io.Serializable;

/**
 *
 * @author Usuario
 */
public class Persona implements  Serializable{

    private String nombres;
    private Direccion direccion;

    public Persona(String nombres, String calles, String ciudad) {
        this.nombres = nombres;
        this.direccion = new Direccion(calles, ciudad);
    }

    /**
     * @return the nombres
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * @param nombres the nombres to set
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * @return the direccion
     */
    public Direccion getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }
    
    

}

class Direccion  implements  Serializable{

    private String calles;
    private String ciuddad;

//constructor
    public Direccion(String calles, String ciuddad) {
        this.calles = calles;
        this.ciuddad = ciuddad;
    }

    /**
     * @return the calles
     */
    public String getCalles() {
        return calles;
    }

    /**
     * @param calles the calles to set
     */
    public void setCalles(String calles) {
        this.calles = calles;
    }

    /**
     * @return the ciuddad
     */
    public String getCiuddad() {
        return ciuddad;
    }

    /**
     * @param ciuddad the ciuddad to set
     */
    public void setCiuddad(String ciuddad) {
        this.ciuddad = ciuddad;
    }

}
