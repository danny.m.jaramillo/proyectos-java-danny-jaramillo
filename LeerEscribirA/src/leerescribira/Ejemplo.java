/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leerescribira;

/**
 *
 * @author Usuario
 */
public abstract class Ejemplo {

    static int otroValor = 4;
    protected int valor;  // caundo son protegidos

//abstracto
    public abstract String imprimir(int valor, String valor2);

    void imprimir() {
        imprimir(3);
    }
//SOBRECARGA

    boolean imprimir(int valor) { //1

        System.out.println(" valor  " + this.valor);
        return true;
    }

    int imprimir(String valor) {  //2
        return 67;
    }

    String imprimir(String valor2, int valor) { //3
        return "j";
    }
//metodo de la calse sattic

    static void metodoClasse() {
        System.out.println(" Metodo de clase " + otroValor);
    }

    //metodos constructorires  sobre carga de constructores
    public Ejemplo() {

    }

    public Ejemplo(int valor) {
        this.valor = valor;

    }

    public Ejemplo(String valor) {

    }
}

class E1 extends Ejemplo {

    @Override
    public String imprimir(int valor, String valor2) {
        this.valor = 4;
        return "";
    }

//    public E1() {
//        this.valor = 5;
//        imprimir();
//
//    }
//
//    //sobreescritura cambiar codigo devuelbe 32 no 67 
//    int imprimir(String valor) {
//        return 32;
//    }
//
//    //sobrecarga mismo nombre diferentes parametros
//    int imprimir(double va) {
//        return 32;
//    }
}

class E2 extends Ejemplo {

    @Override
    public String imprimir(int valor, String valor2) {
        this.valor = 4;
        return "";
    }

    boolean imprimir(int valor) { //1
        this.valor = 6;
        System.out.println(" valor  " + this.valor);
        return true;
    }
}
