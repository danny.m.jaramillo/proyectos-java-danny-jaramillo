/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leerescribira;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class LeerEscribirA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // directorio solo nombre , archico datos.txt  File archivo = new File(" datos.txt");   {sin espacios dentro de las comillas
        File archivo = new File("persona.obj"); 
        creararchivo(archivo);
//        leerArchivo(archivo);
//        escribirArchivo(archivo);
//        leerArchivo(archivo);
        gurdarPersona(archivo, new Persona("Juan Carlos", " AV loja", "Guayaquil"));
        leerPersona(archivo);
        List<Persona> lsita;
        
    }

    static void leerPersona(File archivo) {
        try {
            FileInputStream fs = new FileInputStream(archivo); //leen y escriben objetos en persona
            ObjectInputStream oos = new ObjectInputStream(fs);
            //escribir objetos

            Persona l = (Persona) oos.readObject();
           
            System.out.println("   "+l.getNombres()+"     "+l.getDireccion().getCalles()+"  "+l.getDireccion().getCiuddad());
        } catch (Exception ex) {
            System.out.println(" Error al leer personaa");
            ex.printStackTrace();
        }

    }

    static void gurdarPersona(File archivo, Persona person) {
        try {
            FileOutputStream fs = new FileOutputStream(archivo);
            ObjectOutputStream oos = new ObjectOutputStream(fs);
            //escribir objetos
            oos.writeObject(new ArrayList<Persona>());
            oos.flush();
            oos.close();
        } catch (Exception ex) {
            System.out.println(" Error al guardar persona");
            ex.printStackTrace();
        }
    }

    static void creararchivo(File archivo) {

        boolean existe = archivo.exists();
        System.out.println("Existe " + existe);

        if (!existe) {
            try {
                archivo.createNewFile();
                System.out.println(" Se puede leer? " + archivo.canRead());
                System.out.println(" Se puede escritorio? " + archivo.canWrite());
                System.out.println(" Se puede  ...? " + archivo.canExecute());
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println(" Eror en la creacion dela archivo  ");

            }
            //continua luego del catch

        }

    }

    static void escribirArchivo(File archivo) {
        //FileWriter writer = new FileWriter(archivo, false); elimina lo que ya tiene  , (archivo, true ) agrega

        String[] nombres = {"Carlos", " Rosa", " Miguel", "Pedro"};
        try {
            FileWriter writer = new FileWriter(archivo, true);
            PrintWriter pw = new PrintWriter(writer);

            for (String nombre : nombres) {
                pw.println(nombre);
            }
            pw.flush();//guardar
            pw.close();//cerar    

        } catch (IOException ex) {
            System.out.println(" Error mientras se escribia ");
        }
    }

    static void leerArchivo(File archivo) {
        if (archivo.exists()) {
            try {
                FileReader reader = new FileReader(archivo);
                BufferedReader buff = new BufferedReader(reader);
                String cadena = " ";

                while (cadena != null) {
                    cadena = buff.readLine();
                    if (cadena != null) {
                        System.out.println(cadena);
                    }

                }

            } catch (FileNotFoundException ex) {
                System.out.println(" El archivo no existe ");
            } catch (IOException ex) {
                System.out.println(" Error mientras se leia");
            }
        }
        System.out.println(" Termina de leer");
    }

    static void creardirectorio(File archivo) {

        boolean existe = archivo.exists();
        System.out.println("Existe directorio " + existe);

        if (!existe) {

            archivo.mkdir();
            System.out.println(" Se puede leer?" + archivo.canRead());
            System.out.println(" Se puede escritorio?" + archivo.canWrite());
            System.out.println(" Se puede  ...?" + archivo.canExecute());

        }

    }

}
