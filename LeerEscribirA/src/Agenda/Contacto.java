/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agenda;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class Contacto implements Serializable {

    private String nombre;
    private String telefono;
    private String ciudad;
    private String direccion;

    public Contacto(String nombre, String telefono) {
        this.nombre = nombre;
        this.telefono = telefono;

    }

    public Contacto(String nombre, String telefono, String ciudad, String direccion) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.ciudad = ciudad;
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
// metodos de instancia

    public void imprimir() {
        System.out.println(this.getNombre() + " " + this.getTelefono());
    }

    public void imprimirDetalle() {
        System.out.println(this.getNombre() + " "
                + this.getTelefono() + "  "
                + this.getCiudad() + "   "
                + this.getDireccion() + "  ");
    }

}
