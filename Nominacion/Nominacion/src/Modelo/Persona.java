/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Usuario iTC
 */
public class Persona {

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * @return the cedula
     */
    public Integer getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the ID
     */
    public Integer getID() {
        return ID_persona;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(Integer ID) {
        this.ID_persona = ID;
    }

    /**
     * @return the IDgerente
     */
    public String getIDgerente() {
        return ID_Gerente;
    }

    /**
     * @param IDgerente the IDgerente to set
     */
    public void setIDgerente(String IDgerente) {
        this.ID_Gerente = IDgerente;
    }
    private  String nombre;
    private  String apellido;
    private  Integer cedula;
    private  Integer ID_persona;
    private  String ID_Gerente;

    public Persona(String nombre, String apellido, Integer cedula, Integer ID, String IDgerente) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.ID_persona = ID;
        this.ID_Gerente = IDgerente;
    }

    public Persona() {
    }
    
   
    
}
