/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.List;
import modelo.Asignatura;
import modelo.Estudiante;
import modelo.Docente;

/**
 *
 * @author mac
 */
public class ControladorEstudiante {
    
    public void realizarMatricula() {
    }

    /**
     * Metodo que permite agregar una Asignatura a la lista de asignaturas del
     * Estudiante
     *
     * @param asignatura
     */
    public void agregarAsignatura(Asignatura asignatura) {
        try {
            Estudiante estudiante = new Estudiante();
            // estudiante.setListaAsignatura((List<Asignatura>) asignatura);
            estudiante.listaAsignatura.add(asignatura);
            
        } catch (Exception e) {
            System.out.println("Error al agregar Asignatura");
        }
    }

    /**
     * Metodo que permite agregar Docente a la lista de Docentes del Estudiante
     *
     * @param docente
     */
    public void agregarDocente(Docente docente) {
        Estudiante estudiante = new Estudiante();
        estudiante.getListaDocentes().add(docente);
    }
    
    
}
