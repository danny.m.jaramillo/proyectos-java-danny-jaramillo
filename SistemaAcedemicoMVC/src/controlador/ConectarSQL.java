/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Danny Jaramillo
 */
public class ConectarSQL {

    static Connection coon;
    static final String driver = "com.mysql.jdbc.Driver";
    static final String user = "root";
    static final String paswor = "vismarj2000"; // si tiene contraseña se debe poner la que pusiste en el momento de instalar MYSQL
    static final String url = "jdbc:mysql://localhost/pastilla";

    // nos conectamso a la base de  datos
    public ConectarSQL() {
        coon = null;
        try {
            Class.forName(driver);
            coon = (Connection) DriverManager.getConnection(url, user, paswor);
            // verificar si me conecte o no al Base de datos

            if (coon != null) {
                System.out.println(" CONEXION ESTABLECIDA....");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(" ERROR AL CONECTAR " + e);

        }

    }

    // este metodo nos retorna la coencccion
    public Connection getConnection() {

        return coon;

    }

    // me desconecto con la BASE DE DATOS con este metodo
    public void desconectado() {
        coon = null;

        if (coon == null) {
            System.out.println("-----------------------");
            System.out.println(" Coneccion Terminada..");
        }

    }

}
