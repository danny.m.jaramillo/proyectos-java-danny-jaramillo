/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author mac
 */
public class Asignatura {

    private String nombre;
    private int nroCreditos;
    private int nroHoras;
    private Docente docenteMateria;

    public Asignatura() {
        this.nombre = nombre;
        this.nroCreditos = nroCreditos;
        this.nroHoras = nroHoras;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the nroCreditos
     */
    public int getNroCreditos() {
        return nroCreditos;
    }

    /**
     * @param nroCreditos the nroCreditos to set
     */
    public void setNroCreditos(int nroCreditos) {
        this.nroCreditos = nroCreditos;
    }

    /**
     * @return the nroHoras
     */
    public int getNroHoras() {
        return nroHoras;
    }

    /**
     * @param nroHoras the nroHoras to set
     */
    public void setNroHoras(int nroHoras) {
        this.nroHoras = nroHoras;
    }

    /**
     * @return the docenteMateria
     */
    public Docente getDocenteMateria() {
        return docenteMateria;
    }

    /**
     * @param docenteMateria the docenteMateria to set
     */
    public void setDocenteMateria(Docente docenteMateria) {
        this.docenteMateria = docenteMateria;
    }

}
