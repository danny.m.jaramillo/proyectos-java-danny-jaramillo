/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mac
 */
public class Estudiante extends Persona {

    /**
     * @return the listaDocentes
     */
    public List<Docente> getListaDocentes() {
        return listaDocentes;
    }

    /**
     * @param listaDocentes the listaDocentes to set
     */
    public void setListaDocentes(List<Docente> listaDocentes) {
        this.listaDocentes = listaDocentes;
    }

    private String ciclo;
    public List<Docente> listaDocentes = new ArrayList<>();
    public List<Asignatura> listaAsignatura = new ArrayList<>();
   

    /**
     * Constructor
     */
    public Estudiante() {
    }

    /**
     * Constructor
     *
     * @param ciclo
     */
    public Estudiante(String ciclo) {
        this.ciclo = ciclo;
    }

    /**
     * @return the ciclo
     */
    public String getCiclo() {
        return ciclo;
    }

    /**
     * @param ciclo the ciclo to set
     */
    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    /**
     * @return the listaDocentes
     */
//    public List<Docente> getListaDocentes() {
//        return listaDocentes;
//    }
//
//    /**
//     * @param listaDocentes the listaDocentes to set
//     */
//    public void setListaDocentes(List<Docente> listaDocentes) {
//        this.listaDocentes = listaDocentes;
//    }

    /**
     * @return the listaAsignatura
     */
    public List<Asignatura> getListaAsignatura() {
        return listaAsignatura;
    }

    /**
     * @param listaAsignatura the listaAsignatura to set
     */
    public void setListaAsignatura(List<Asignatura> listaAsignatura) {
        this.listaAsignatura = listaAsignatura;
    }

    /**
     * @return the listaEstudiante
     */
//    public List<Estudiante> getListaEstudiante() {
//        return listaEstudiante;
//    }
//
//    /**
//     * @param listaEstudiante the listaEstudiante to set
//     */
//    public void setListaEstudiante(List<Estudiante> listaEstudiante) {
//        this.listaEstudiante = listaEstudiante;
//    }

}
