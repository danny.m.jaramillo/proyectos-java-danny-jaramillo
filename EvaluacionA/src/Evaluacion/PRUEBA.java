/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluacion;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Danny Jaramillo
 */
public class PRUEBA {

    public static void main(String[] args) {

        Scanner pedir = new Scanner(System.in);
        PRUEBA a = new PRUEBA();
        System.out.println("---------------------------------------------");
        System.out.println("[ Ingrese la longitud  de la matriz cuadrada ]");
        System.out.println("---------------------------------------------");
        int valor = pedir.nextInt();

        int cont = 0;
        System.out.println("--------------------------------");
        System.out.println("[ La primera matriz generada es ]");
        System.out.println("--------------------------------");
        System.out.println(" ");

        int[][] A = new int[valor][valor];
        int[][] B = new int[valor][valor];
        List<Integer> lA = new ArrayList<Integer>();
        List<Integer> lB = new ArrayList<Integer>();

        for (int i = 0; i < valor; i++) {
            for (int j = 0; j < valor; j++) {
                int y;
                A[i][j] = (int) Math.floor(Math.random() * (8 - 0)) + 0;
                y = A[i][j];
                lA.add(y);

                System.out.print(" " + A[i][j] + "  ");

                if (cont == (valor - 1)) {

                    System.out.println("  ");
                }
                cont++;
            }
            cont = 0;

        }
        System.out.println("");
        System.out.println("--------------------------");
        System.out.println("[  La segunda matriz es  ]:");
        System.out.println("--------------------------");

        System.out.println(" ");

        for (int i = 0; i < valor; i++) {
            for (int j = 0; j < valor; j++) {
                int y;
                B[i][j] = (int) Math.floor(Math.random() * (8 - 0)) + 0;
                y = B[i][j];
                lB.add(y);

                System.out.print(" " + B[i][j] + "  ");

                if (cont == (valor - 1)) {

                    System.out.println("  ");
                }
                cont++;
            }
            cont = 0;

        }

        List<Integer> lAB = new ArrayList<Integer>();
        System.out.println(" ");
        System.out.println("------------------------------------------------------");
        System.out.println("[   Los elementos de la matriz resultante son (A-B)  ]");
        System.out.println("------------------------------------------------------");
        System.out.println("");

        for (int i = 0; i < lA.size(); i++) {

            if (lB.contains(lA.get(i))) {
         // no lo agrego
            } else {
           // lo agrego
                lAB.add(lA.get(i));
            }

        }

        for (int i = 0; i < lAB.size(); i++) {

            System.out.println("* " + lAB.get(i));

        }
        System.out.println("");
        System.out.println("---------------------------------------------");
    }

}
