/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BuscadorAcademico;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class Estudiante extends Persona {

    private String ciclo;
    private List<Asignatura> listadoAsiganturas = new ArrayList<Asignatura>();
    private List<Docente> listadoDocente = new ArrayList<Docente>();

    /**
     * Constructor
     */
    public Estudiante() {

    }

    /**
     * Constructor
     *
     * @param ciclo
     */
    public Estudiante(String ciclo) {

        this.ciclo = ciclo;

    }

    public void realizarMatricula() {

    }

    /**
     * Agrego Asignaturas a la lista
     *
     * @param asignatura
     */
    public void agregarAsignatura(Asignatura asignatura) {
        this.listadoAsiganturas.add(asignatura);
    }

    /**
     * Agregar un docenet a la lista
     *
     * @param docente
     */
    public void agregarDocente(Docente docente) {
        this.listadoDocente.add(docente);
    }

    /**
     * @return the ciclo
     */
    public String getCiclo() {
        return ciclo;
    }

    /**
     * @param ciclo the ciclo to set
     */
    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    /**
     * @return the listadoAsiganturas
     */
    public List<Asignatura> getListadoAsiganturas() {
        return listadoAsiganturas;
    }

    /**
     * @param listadoAsiganturas the listadoAsiganturas to set
     */
    public void setListadoAsiganturas(List<Asignatura> listadoAsiganturas) {
        this.listadoAsiganturas = listadoAsiganturas;
    }

    /**
     * @return the listadoDocente
     */
    public List<Docente> getListadoDocente() {
        return listadoDocente;
    }

    /**
     * @param listadoDocente the listadoDocente to set
     */
    public void setListadoDocente(List<Docente> listadoDocente) {
        this.listadoDocente = listadoDocente;
    }

}
