/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BuscadorAcademico;

/**
 *
 * @author Usuario
 */
public class Asignatura {

    private String nombremateria;
    private int numerohoras;
    private int numerodecreditos;

    public Asignatura(String nombre, int numerohoras, int numerocreditos) {
        this.nombremateria = nombre;
        this.numerohoras = numerohoras;
        this.numerodecreditos = numerodecreditos;

    }

    public void AgregarAsigantura() {

    }

    /**
     * @return the materia
     */
    public String getMateria() {
        return nombremateria;
    }

    /**
     * @param materia the materia to set
     */
    public void setMateria(String materia) {
        this.nombremateria = materia;
    }

    /**
     * @return the numerohoras
     */
    public int getNumerohoras() {
        return numerohoras;
    }

    /**
     * @param numerohoras the numerohoras to set
     */
    public void setNumerohoras(int numerohoras) {
        this.numerohoras = numerohoras;
    }

    /**
     * @return the numerodecreditos
     */
    public int getNumerodecreditos() {
        return numerodecreditos;
    }

    /**
     * @param numerodecreditos the numerodecreditos to set
     */
    public void setNumerodecreditos(int numerodecreditos) {
        this.numerodecreditos = numerodecreditos;
    }

}
