/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Danny Vasquez
 */
public class Cargo {
    private String trabajoARealizar;
    private String cargaHoraria;
    private String Oficina;
    private String idCarga;
    
    
    public Cargo(){
        
    }

    /**
     * @return the trabajoARealizar
     */
    public String getTrabajoARealizar() {
        return trabajoARealizar;
    }

    /**
     * @param trabajoARealizar the trabajoARealizar to set
     */
    public void setTrabajoARealizar(String trabajoARealizar) {
        this.trabajoARealizar = trabajoARealizar;
    }

    /**
     * @return the cargaHoraria
     */
    public String getCargaHoraria() {
        return cargaHoraria;
    }

    /**
     * @param cargaHoraria the cargaHoraria to set
     */
    public void setCargaHoraria(String cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    /**
     * @return the Oficina
     */
    public String getOficina() {
        return Oficina;
    }

    /**
     * @param Oficina the Oficina to set
     */
    public void setOficina(String Oficina) {
        this.Oficina = Oficina;
    }

    /**
     * @return the idCarga
     */
    public String getIdCarga() {
        return idCarga;
    }

    /**
     * @param idCarga the idCarga to set
     */
    public void setIdCarga(String idCarga) {
        this.idCarga = idCarga;
    }

    
    
    
}
