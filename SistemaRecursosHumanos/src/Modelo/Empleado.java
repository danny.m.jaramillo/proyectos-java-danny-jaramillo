/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Danny Vasquez
 */
public class Empleado {
    private String nombre;
    private String apellido;
    private String CI;
    private String idEmpleado;
    private String idGerente;
    private String direccion;
    private String sueldo;
            
    
    
    private List<Cargo> cargoEmpleado = new ArrayList<>();

    public Empleado(){
        
    }
    
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * @return the CI
     */
    public String getCI() {
        return CI;
    }

    /**
     * @param CI the CI to set
     */
    public void setCI(String CI) {
        this.CI = CI;
    }

    /**
     * @return the idEmpleado
     */
    public String getIdEmpleado() {
        return idEmpleado;
    }

    /**
     * @param idEmpleado the idEmpleado to set
     */
    public void setIdEmpleado(String idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    /**
     * @return the idGerente
     */
    public String getIdGerente() {
        return idGerente;
    }

    /**
     * @param idGerente the idGerente to set
     */
    public void setIdGerente(String idGerente) {
        this.idGerente = idGerente;
    }

    /**
     * @return the cargoEmpleado
     */
    public List<Cargo> getCargoEmpleado() {
        return cargoEmpleado;
    }

    /**
     * @param cargoEmpleado the cargoEmpleado to set
     */
    public void setCargoEmpleado(List<Cargo> cargoEmpleado) {
        this.cargoEmpleado = cargoEmpleado;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the sueldo
     */
    public String getSueldo() {
        return sueldo;
    }

    /**
     * @param sueldo the sueldo to set
     */
    public void setSueldo(String sueldo) {
        this.sueldo = sueldo;
    }

    
    
}
