/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Danny Vasquez
 */
public class Empresa {
    private String nombreEmpresa = "Recursos Humanos Loja";
    private String idEmpresa = "01112";
    private String direccion = "Loja";
    private int telefono;
    private List<Empleado> persona = new ArrayList<>();
    
    private List<Cargo> oficina = new ArrayList<>();
    public Empresa(){
        
    }
    /**
     * @return the nombreEmpresa
     */
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    /**
     * @param nombreEmpresa the nombreEmpresa to set
     */
    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    /**
     * @return the idEmpresa
     */
    public String getIdEmpresa() {
        return idEmpresa;
    }

    /**
     * @param idEmpresa the idEmpresa to set
     */
    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    /**
     * @return the persona
     */
    public List<Empleado> getPersona() {
        return persona;
    }

    /**
     * @param persona the persona to set
     */
    public void setPersona(List<Empleado> persona) {
        this.persona = persona;
    }

    /**
     * @return the oficina
     */
    public List<Cargo> getOficina() {
        return oficina;
    }

    /**
     * @param oficina the oficina to set
     */
    public void setOficina(List<Cargo> oficina) {
        this.oficina = oficina;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the telefono
     */
    public int getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    
    
    
    
}
