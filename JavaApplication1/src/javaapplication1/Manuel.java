/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.util.Scanner;
import static javaapplication1.Ejecutor_Peticiones.convertirMayusculas;

/**
 *
 * @author Usuario
 */
public class Manuel {

    public static void main(String[] args) {
        //METODO PARA TRANSFORMAR UN STRING ES MAYUSCULAS
        
        Scanner lector = new Scanner(System.in);
        System.out.println(" Ingrese una palabra");
        System.out.println("---------------------");
        String palabra1 = lector.next();

        System.out.println("Transformacion de la palabra en mayusculas : " + palabra1.toUpperCase());

        
        //METODO PARA TRANSFORMAR UN STRING ES MINUSCULAS
        System.out.println("   ");
        
        System.out.println(" Ingrese la palabra en Mayusculas");
        System.out.println("---------------------------------");
        String palabra2 = lector.next();
        System.out.println(" Transforamcion de una palabra a minnusculas : " + palabra2.toLowerCase());
        
       
        //METODO PARA REMPLAZAR A POR *
        System.out.println("   ");
        
        System.out.println(" Ingrese la palabra ");
        System.out.println("---------------------------------");
        String palabra3 = lector.next();
        String conversion = palabra3.replaceAll("a","*");
        System.out.println(" Remplazado: "+conversion);

    }

}
