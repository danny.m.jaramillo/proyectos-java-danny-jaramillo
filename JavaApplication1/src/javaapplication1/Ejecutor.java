/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.text.DecimalFormat;
import javax.swing.UIManager;

/**
 *
 * @author juanxxiii
 */
public class Ejecutor {

    public static void main(String[] args) {
        //Println
        /*System.out.println("HOLA MUNDO");
         System.out.println("DESDE JAVA");

         //Print
         System.out.print("Hola mundo ");
         System.out.print("desde java");
         */
        //******** TIPOS DE DATOS INT Y DOUBLE *********;
        //===== EJERCICIO 1 =========

//        int numero1 = 4;
//        int numero2 = 5;
//        int suma = 0;
//        suma = numero1 + numero2;
        //System.out.println("la suma es = " + suma);
        //System.out.println("LA SUMA ES = "+(numero1+numero2));

        //===== EJERCICIO 2 =========
//        double dividendo = 8;
//        double divisor = 7;
//        double respuesta = dividendo / divisor;
//        DecimalFormat formato = new DecimalFormat("#.####");
//        //System.out.println("LA DIVICION ES = "+ formato.format(respuesta));

        //******** TIPOS DE DATOS STRING *********;
        //Ejercico1
        String nombre = "Diego";
       System.out.println(nombre.equals("diego"));
        System.out.println(nombre.equalsIgnoreCase("diego"));
        
        System.out.println("HOLA MUNDO");

        //Ejercicio2
       // String informacion="Diego,Guaman,23 años, die1918@gmail.com";
        //String datos[]=informacion.split(",");
         //System.out.println("tamaño de vector "+datos.length);
        //for(int i=0;i<datos.length;i++){
        //System.out.println("Posicion "+i+" : "+datos[i]);
        /********* TIPOS DE DATOS CHAR ***********
        char caracter = 'A';
        char caracter2 = 'D';
        char caracter3 = 92;

        int sumaCaracter = caracter + caracter2;
        System.out.println("la suma del caracter es " + sumaCaracter);
         System.out.println("el codogo ASCCI  es= " + caracter3);
//
//        //************CASTING***************
//        //CASTING: convertir de un tipo de DATO a otro;
//        //==Ejercicio 1 STRING A INT====
//        String numString1 = "20";
//        String numString_2 = "40";
//        int numInt = 10;
//        int respSumaCasting = 0;
//        respSumaCasting = Integer.parseInt(numString1) + Integer.parseInt(numString_2) + numInt;
//       // System.out.println("la respuesta es = " + respSumaCasting);
//
//        //=====Ejercicio 2 STRING A DOUBLE====
//        String numDouble1 = "8.9";
//        String numDouble2 = "4.5";
//        double numDouble3 = 2.4;
//
//        double respSumDouble = Double.parseDouble(numDouble1) + Double.parseDouble(numDouble2) + numDouble3;
//        //System.out.println(" la suma es = " + respSumDouble);
//
//        //======= EJERCICO 3 ======
//        String numString_1 = "20";
//        String numString2 = "40";
//        int numInt_ = 10;
//        String respSumaCasting_ = "";
//        respSumaCasting_ = numString_1 + numString2 + numInt_ + "";
//        //System.out.println("la respuesta es = " + respSumaCasting_);
//        respSumaCasting_ = numString_1 + numString2 + numInt_;
//
//        //********LLAMAR A ATRIBUTOS DE UNA CLASE*********
//        Persona persona = new Persona();
//        //System.out.println(persona.nombre);// ESTO NO ES ANONIMO.
//        //System.out.println(new Persona().nombre);// ESTO ES ANONIMO.
//
///////*****CAMBIAR EL VALOR DE MI ATRIBUTO*************
//        Persona persona1 = new Persona();
//        String nuevoNombre = persona1.nombre;
//        //System.out.println(nuevoNombre);
//        persona1.nombre = "Juan";
//       // System.out.println(persona1.nombre);
//
//        //**************/////
//        Persona casa = new Persona();
//        //System.out.println(casa.apellido);//no anonimo
//        // System.out.println(new Persona().apellido);//anonimo
//        // System.out.println(casa.edad); //no anonimo
//        // System.out.println(new Persona().edad);//anonimo
//        // System.out.println(casa.sexo);
//        // System.out.println(new Persona().sexo);//anonimo
//        // System.out.println(casa.numeroCedula);
//        //System.out.println(new Persona().numeroCedula);//anonimo
//
//        //*****///
//        Persona casa2 = new Persona();
//        // String otroApellido=casa2.apellido;
//        //System.out.println(otroApellido);
//        casa2.apellido_ = "Gualan";
//        // System.out.println(casa2.apellido);
//
//        Persona numero_ = new Persona();
//        //System.out.println(numero_.numero1);
//        // System.out.println(numero_.numero2);
//
//        int respSumaCasting4 = 0;
//        respSumaCasting4 = Integer.parseInt(numero_.numero1) + Integer.parseInt(numero_.numero2);
//        //System.out.println("la suma es "+respSumaCasting4 );
//
//        Persona persona5 = new Persona();
//       // System.out.println("Apellido = " + persona5.getApellido());
//        // System.out.println("Edad = " + persona5.getEdad());
//
//        //System.out.println("****NUEVOS VALORES 1****");
//        persona5.setApellido("Jima");
//        persona5.setEdad(24);
//        //System.out.println("Apellido = " + persona5.getApellido());
//        // System.out.println("Edad" + persona5.getEdad());
//
//        // System.out.println("****NUEVOS VALORES 2****");
//        persona5.setApellido("Valarezo");
//        persona5.setEdad(25);
//       // System.out.println("Apellido = " + persona5.getApellido());
//        //System.out.println("Edad" + persona5.getEdad());
//
//        Estructura_Rep_Cond estru = new Estructura_Rep_Cond();
//       // estru.primerMetodoSinRetorno();
//       /// estru.segundoMetodoSinRetorno();
//       // estru.metodoConParametro(3);
//        //estru.SegundoMetodoConParametro("María", 1.55);
//       //estru.metodo("carlos");
//       
//       
//       
//       Estructura_Rep_Cond decimal=new Estructura_Rep_Cond();
//       //decimal.FormatearDecimales(12.36, 6.0);
//       
//       
//       /////probando Metodos con retorno////**
//        //System.out.println("la suma es = " +decimal.retornarSuma(2, 2)); 
//       
//       // double a=100;
//        //System.out.println("la suma es = "+(decimal.retorSuma("3.3","3.3")+a));
//        
//        Estructura_Rep_Cond contar=new Estructura_Rep_Cond();
//        //contar.Incremento(20);
//        //contar.probar_do_while();
//       //contar.probar_vector_do_while();
//       //contar.probar_vector_dowhile();
//        //contar.numeroEntero(10);
//        
//       // ****** ESTRUCTURAS CONDICIONALES *** SWICH Y CASE *******
//       Estructura_Rep_Cond meses=new Estructura_Rep_Cond();
//       //meses.Practica_Switch_Case(6);
//        //meses.diasSemana("MArtes");
//       //meses.vocales('l');
//      // meses.recorrerCaracter("Maria");
//        //Persona entero=new Persona();
//        //entero.devolverEntero(5);
//       
//       
//       Persona pares=new Persona();
//       pares.devolver(4, 5);
    }
}

