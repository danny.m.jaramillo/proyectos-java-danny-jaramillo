/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delfin;

/**
 *
 * @author Usuario
 */
public class animal {

    private int edad;
    private int peso;
    private String nombre;
    private int altura;
    String movimiento;

    /**
     * @return the edad
     */
    //Encapsulacion 
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * @return the peso
     */
    public int getPeso() {
        return peso;
    }

    /**
     * @param peso the peso to set
     */
    public void setPeso(int peso) {
        this.peso = peso;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the altura
     */
    public int getAltura() {
        return altura;
    }

    /**
     * @param altura the altura to set
     */
    public void setAltura(int altura) {
        this.altura = altura;
    }

    // Concepto
    // SOBRECARGA  la sobrecarga se da debido ah que tenemos  metodos con el mismo nombre pero con diferentes argumentos o parametros
    // no pueden existir dos metodos con el mismo nombre y con los mismos parametros o argumentos en la misma clase
    public void moverse() {

        System.out.println(" El animal de mueve : " + movimiento);

    }

//Sobrecarga 1
    // existe sobrecarga en el siguiente metodo yaque me resivira  un parametro del nombre del animal 
    public void moverse(String nombre) {

        System.out.println(" El animal de mueve  : " + movimiento);

    }

    //Sobrecarga 2
// El segundo ejemplo de sobrecarga trata de presentar las medidas del animal em cuestion 
    public void medidas(int altura) {

        System.out.println(" El animal mide " + altura);

    }

    public void medidas() {

        System.out.println(" El animal mide " + this.getAltura());

    }
}

class delfin extends animal {
// variable local de la clase delfin

    int cm = 0;
//Sobreescritura 1
    //Concepto
//La sobreescritura se da cuanto un metodo que es heredado de la clase padre a la clase hija , no cambia sus parametros y su tipo de retorno , unicamente la aprte interna del metodo , (no es obligatorio modificarla ) sera una sobreescritura igualemnte sino modificamos nada en al calse hija
    // en mi caso utilice la sobreescritua par el metodo moverse()  , en el cual  hare una condicion si el animal es delfin se movera nadando y presentare nadando  

    public void moverse(String nombre) {
        if (nombre.equals("delfin")) {
            movimiento = "nadando";
            System.out.println(" El delfin se  mueve  : " + this.movimiento);
        }

    }

    //Sobreescritura 2
    // la altura unicamente se ingresara en metros desde consola
    // el segundo ejemplo de sobreescritura trata de transformar la medida de los animales en  cm 
    // ypara mas exactitud unicamente cantidades enteras
    public void medidas(int altura) {

        cm = altura * 100;
        System.out.println(" La medaid del animal  en cm  es :  " + altura);

    }

    public static void main(String[] args) {

    }
}
