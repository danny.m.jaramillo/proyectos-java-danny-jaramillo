/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscadoracademico3a;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mac
 */
public class Estudiante extends Persona {

    private String ciclo;
    private List<Docente> listaDocentes = new ArrayList<>();
    private List<Asignatura> listaAsignatura = new ArrayList<>();

    /**
     * Constructor
     */
    public Estudiante(){
    }
    /**
     * Constructor
     * @param ciclo 
     */
    public Estudiante(String ciclo){
        this.ciclo=ciclo;
    }
    
    public void realizarMatricula() {
    }
    /**
     * Metodo que permite agregar una Asignatura a la lista 
     * de asignaturas del Estudiante
     * @param asignatura 
     */
    public void agregarAsignatura(Asignatura asignatura){
        this.listaAsignatura.add(asignatura);
    }
    
    /**
     * Metodo que permite agregar Docente a la lista de Docentes
     * del Estudiante
     * @param docente 
     */
    public void agregarDocente(Docente docente){
        this.listaDocentes.add(docente);
    }

    /**
     * @return the ciclo
     */
    public String getCiclo() {
        return ciclo;
    }

    /**
     * @param ciclo the ciclo to set
     */
    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    /**
     * @return the listaDocentes
     */
    public List<Docente> getListaDocentes() {
        return listaDocentes;
    }

    /**
     * @param listaDocentes the listaDocentes to set
     */
    public void setListaDocentes(List<Docente> listaDocentes) {
        this.listaDocentes = listaDocentes;
    }

    /**
     * @return the listaAsignatura
     */
    public List<Asignatura> getListaAsignatura() {
        return listaAsignatura;
    }

    /**
     * @param listaAsignatura the listaAsignatura to set
     */
    public void setListaAsignatura(List<Asignatura> listaAsignatura) {
        this.listaAsignatura = listaAsignatura;
    }

}
