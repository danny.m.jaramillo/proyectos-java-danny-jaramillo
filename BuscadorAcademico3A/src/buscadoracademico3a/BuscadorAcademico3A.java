/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscadoracademico3a;

/**
 *
 * @author mac
 */
public class BuscadorAcademico3A {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Asignatura objAsignaturaProgramacion = new Asignatura("Programacion II", 10, 120);
        Asignatura objAsignaturaFisica = new Asignatura("Fisica", 4, 80);
        Asignatura objAsignaturaBaseDatosI = new Asignatura("Base de Datos I", 5, 90);

        Docente objDocenteXavier = new Docente("Ingeniero Xavier");
        Docente objDocenteRoberth = new Docente("Ingeniero Roberth");
        Docente objDocenteMaria = new Docente("Ingeniera Maria");

        Estudiante objEstudianteDoris = new Estudiante("Tercero A");
        Estudiante objEstudianteAlex = new Estudiante("Tercero B");
        Estudiante objEstudianteJuanPablo = new Estudiante("Tercero C");

        objEstudianteDoris.setNombre("Doris");
        objEstudianteAlex.setNombre("Alex");
        objEstudianteJuanPablo.setNombre("Juan Pablo");

        objEstudianteDoris.agregarAsignatura(objAsignaturaProgramacion);
        objEstudianteDoris.agregarAsignatura(objAsignaturaFisica);
        objEstudianteDoris.agregarDocente(objDocenteXavier);

        objEstudianteAlex.agregarAsignatura(objAsignaturaProgramacion);
        objEstudianteAlex.agregarAsignatura(objAsignaturaFisica);
        objEstudianteAlex.agregarAsignatura(objAsignaturaBaseDatosI);
        objEstudianteAlex.agregarDocente(objDocenteRoberth);
        objEstudianteAlex.agregarDocente(objDocenteXavier);
        objEstudianteAlex.agregarDocente(objDocenteMaria);

        objEstudianteJuanPablo.agregarAsignatura(objAsignaturaBaseDatosI);
        objEstudianteJuanPablo.agregarDocente(objDocenteMaria);

        //Presentacion Estudiante Doris
        System.out.println("==PRESENTACION DE INFORMACION DORIS==");
        System.out.println("Estudiante:" + objEstudianteDoris.getNombre());
        for (Asignatura elemento : objEstudianteDoris.getListaAsignatura()) {
            System.out.println("Contenido del Asignaturas:" + elemento.getNombre());
        }
        objEstudianteDoris.getListaDocentes().forEach(elemento -> System.out.println("Los Docentes:" + elemento.getTitulo()));

        //Presentacion Estudiante Alex
        System.out.println("==PRESENTACION DE INFORMACION ALEX==");
        System.out.println("Estudiante:" + objEstudianteAlex.getNombre());
        for (Asignatura elemento : objEstudianteAlex.getListaAsignatura()) {
            System.out.println("Contenido del Asignaturas:" + elemento.getNombre());
        }
        objEstudianteAlex.getListaDocentes().forEach(elemento -> System.out.println("Los Docentes:" + elemento.getTitulo()));

        //Presentacion Estudiante Juan Pablo
        System.out.println("==PRESENTACION DE INFORMACION JUAN PABLO==");
        System.out.println("Estudiante:" + objEstudianteJuanPablo.getNombre());
        for (Asignatura elemento : objEstudianteJuanPablo.getListaAsignatura()) {
            System.out.println("Contenido del Asignaturas:" + elemento.getNombre());
        }
        objEstudianteJuanPablo.getListaDocentes().forEach(elemento -> System.out.println("Los Docentes:" + elemento.getTitulo()));

    }

}
