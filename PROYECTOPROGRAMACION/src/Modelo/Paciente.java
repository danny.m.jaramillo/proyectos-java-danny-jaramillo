/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Danny Jaramillo
 */
public class Paciente {

    private String nombre;
    private int edad;
    private String estadosivil;

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * @return the estadosivil
     */
    public String getEstadosivil() {
        return estadosivil;
    }

    /**
     * @param estadosivil the estadosivil to set
     */
    public void setEstadosivil(String estadosivil) {
        this.estadosivil = estadosivil;
    }

}
