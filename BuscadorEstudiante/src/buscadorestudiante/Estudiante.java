/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscadorestudiante;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Usuario iTC
 */
public class Estudiante {

    /**
     * @return the listaDocentes
     */
    public List<String> getListaDocentes() {
        return listaDocentes;
    }

    /**
     * @param listaDocentes the listaDocentes to set
     */
    public void setListaDocentes(List<String> listaDocentes) {
        this.listaDocentes = listaDocentes;
    }

    /**
     * @return the listaMateria
     */
    public List<String> getListaMateria() {
        return listaMateria;
    }

    /**
     * @param listaMateria the listaMateria to set
     */
    public void setListaMateria(List<String> listaMateria) {
        this.listaMateria = listaMateria;
    }

    /**
     * Encapsulo los atributos de la clase
     */
    private String nombre;
    private String cedula;
    private String ciclo;
    private String paralelo;

    private List<String> listaDocentes = new ArrayList<>();
    private List<String> listaMateria = new ArrayList<>();

    /**
     * Constructor con parametros
     */
    public Estudiante(String nom, String ced, String cicl, String parale) {

        this.nombre = nom;
        this.cedula = ced;
        this.ciclo = cicl;
        this.paralelo = parale;

    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the ciclo
     */
    public String getCiclo() {
        return ciclo;
    }

    /**
     * @param ciclo the ciclo to set
     */
    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    /**
     * @return the paralelo
     */
    public String getParalelo() {
        return paralelo;
    }

    /**
     * @param paralelo the paralelo to set
     */
    public void setParalelo(String paralelo) {
        this.paralelo = paralelo;
    }

   

}
