/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscador_completo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Danny Jaramillo
 */
public class esttudiante {
    
    
    private String nombre;
    private String cedula;
    private String edad;
    private String Direccion;

    private List<String> listaDocentes = new ArrayList<>();
    private List<String> listaMateria = new ArrayList<>();

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the edad
     */
    public String getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(String edad) {
        this.edad = edad;
    }

    /**
     * @return the Direccion
     */
    public String getDireccion() {
        return Direccion;
    }

    /**
     * @param Direccion the Direccion to set
     */
    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    /**
     * @return the listaDocentes
     */
    public List<String> getListaDocentes() {
        return listaDocentes;
    }

    /**
     * @param listaDocentes the listaDocentes to set
     */
    public void setListaDocentes(List<String> listaDocentes) {
        this.listaDocentes = listaDocentes;
    }

    /**
     * @return the listaMateria
     */
    public List<String> getListaMateria() {
        return listaMateria;
    }

    /**
     * @param listaMateria the listaMateria to set
     */
    public void setListaMateria(List<String> listaMateria) {
        this.listaMateria = listaMateria;
    }

    
    
    

}
