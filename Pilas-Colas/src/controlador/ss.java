/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.Stack;

/**
 *
 * @author Danny Jaramillo
 */
public class ss {
    // para hacer pilas
    //La clase Stack es una clase de las llamadas de tipo LIFO (Last In - First Out, o último en entrar - primero en salir). Esta clase hereda de la clase que ya hemos estudiado anteriormente en el curso Vector y con 5 operaciones permite tratar un vector a modo de pila o stack.

//Las operaciones básicas son push (que introduce un elemento en la pila), pop (que saca un elemento de la pila), peek (consulta el primer elemento de la cima de la pila), empty (que comprueba si la pila está vacía) y search (que busca un determinado elemento dentro de la pila y devuelve su posición dentro de ella).
    public static void main(String[] args) {

        Stack<String> pila = new Stack<String>();
        System.out.println(" Presento ");
        for (int x = 1; x <= 10; x++) {
            System.out.println(" "+x);
            pila.push(Integer.toString(x));
        }
        
        System.out.println(" LEOO");

        while (!pila.empty()) {
            System.out.println(pila.pop());
        }

    }

    
}
