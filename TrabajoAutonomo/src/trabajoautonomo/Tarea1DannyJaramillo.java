/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabajoautonomo;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class Tarea1DannyJaramillo {

    Scanner pantalla = new Scanner(System.in);

    public static void main(String[] args) {

        boolean el = true;
        Scanner pantalla = new Scanner(System.in);
        Tarea1DannyJaramillo a = new Tarea1DannyJaramillo();
        do {
            System.out.println("********************************************");
            System.out.println("****************BIENVENIDO******************");
            System.out.println("********************************************");
            System.out.println("*                                           *");
            System.out.println("* Escoja una de las sigueintes opciones .  *");
            System.out.println("* Ordenar : Elementos de un vector.        *");
            System.out.println("* Operaciones: Con elementos de un vector. *");
            System.out.println("* Saludo : Primera imprecion en pantalla.  *");
            System.out.println("********************************************");

            String res = pantalla.next();
            if (res.equalsIgnoreCase("ordenar")) {
                a.ordenar();
            } else if (res.equalsIgnoreCase("operaciones")) {
                a.operaciones();
            } else if (res.equalsIgnoreCase("saludo")) {
                a.saludo();
            } else {
                System.out.println("La opcion ingresada no es Correcta");
            }
            System.out.println("");
            System.out.println("");
            System.out.println("------------------------------");
            System.out.println("|Decea realizar otra operacion|");
            System.out.println("|[0]Si                        |");
            System.out.println("|[1]No                        |");
            System.out.println("------------------------------");
            int l = pantalla.nextInt();

            if (l == 1) {
                el = false;
            }
        } while (el);
        System.out.println(" ---------------------- ");
        System.out.println(" !Gracias por su vicita!");
        System.out.println(" ----------------------");
        System.out.println("|Autor: Danny Jaramillo|");
        System.out.println("------------------------");
    }

    public void saludo() {

        System.out.println("-------------");
        System.out.println("| Hola Mundo |");
        System.out.println("-------------");

    }

    public void ordenar() {

        int[] numeros = new int[15];
        int num = 0;
        int respuesta;
        for (int i = 0; i < 15; i++) {
            System.out.println("---------------------");
            System.out.println("|Ingrese un numero :|");
            System.out.println("---------------------");
            num = pantalla.nextInt();

            numeros[i] = num;
        }
        System.out.println("");
        //presentamos el vector desordenado
        System.out.println("-------------------------------------------------");
        System.out.println("|El vector generado por los numeros ingresados es|");
        System.out.println("-------------------------------------------------");
        System.out.println(" ");
        for (int i = 0; i < 15; i++) {

            System.out.print(" " + numeros[i] + "  ");
        }

        System.out.println(" ");
        System.out.println(" ");
        System.out.println("------------------------------------");
        System.out.println("|Decea ordenar el vector de manera :|");
        System.out.println("| [0] Ascendente                    |");
        System.out.println("| [1] Descendente                   |");
        System.out.println("------------------------------------");

        respuesta = pantalla.nextInt();
        int aux;
        int cont = 1;
        if (respuesta == 0) {

            for (int j = 14; 0 < j; j--) {

                for (int i = 0; i < j; i++) {
                    if (numeros[i] > numeros[cont]) {
                        aux = numeros[i];
                        numeros[i] = numeros[cont];
                        numeros[cont] = aux;
                    }
                    cont++;
                }
                cont = 1;

            }
            System.out.println(" El vector ordenado dela forma Ascendente termino de la siguiente manera :");
            for (int i = 0; i < 15; i++) {

                System.out.print(" " + numeros[i] + "  ");
            }

        } else if (respuesta == 1) {

            for (int j = 14; 0 < j; j--) {

                for (int i = 0; i < j; i++) {
                    if (numeros[i] < numeros[cont]) {
                        aux = numeros[i];
                        numeros[i] = numeros[cont];
                        numeros[cont] = aux;
                    }
                    cont++;
                }
                cont = 1;

            }
            System.out.println(" El vector ordenado dela forma Descendente termino de la siguiente manera :");

            for (int i = 0; i < 15; i++) {

                System.out.print(" " + numeros[i] + "  ");
            }
        } else {
            System.out.println(" ! El valor ingresado no es correcto porfavor ingresar un numero correcto !");
        }

    }

    public void operaciones() {

        int[] numeros = new int[10];
        int num = 0;
        int suma = 0;

        int multiplicacion = 1;

        for (int i = 0; i < 10; i++) {
            System.out.println("---------------------");
            System.out.println("|Ingrese un numero :|");
            System.out.println("---------------------");
            num = pantalla.nextInt();

            numeros[i] = num;
        }
        System.out.println("");

        System.out.println("-------------------------------------------------");
        System.out.println("|El vector generado por los numeros ingresados es|");
        System.out.println("-------------------------------------------------");
        System.out.println(" ");
        for (int i = 0; i < 10; i++) {
            suma = suma + numeros[i];
            multiplicacion = multiplicacion * numeros[i];
            System.out.print(" " + numeros[i] + "  ");

        }
        int resta = numeros[0];
        double divicion = numeros[0];
        //Resta 
        for (int i = 1; i < 10; i++) {

            resta = resta - numeros[i];

        }
        // divicion 

        for (int i = 1; i < 10; i++) {

            divicion = divicion / numeros[i];

        }
        System.out.println("");
        System.out.println("--------------------------");
        System.out.println("| El resultado es :   ");
        System.out.println("| Suma = " + suma + "     ");
        System.out.println("| Resta = " + resta + "     ");
        System.out.println("| Divicion = " + divicion + "     ");
        System.out.println("| Multiplicacion = " + multiplicacion + "     ");
        System.out.println("---------------------------");
    }

}
